import type { CstNode, ICstVisitor, IToken } from 'chevrotain';

export interface ProgrammCstNode extends CstNode {
  name: 'programm';
  children: ProgrammCstChildren;
}

export type ProgrammCstChildren = {
  global_directive?: Global_directiveCstNode[];
  global_declaration?: Global_declarationCstNode[];
};

export interface Global_directiveCstNode extends CstNode {
  name: 'global_directive';
  children: Global_directiveCstChildren;
}

export type Global_directiveCstChildren = {
  diagnostic_directive?: Diagnostic_directiveCstNode[];
  enable_directive?: Enable_directiveCstNode[];
  requires_directive?: Requires_directiveCstNode[];
  Comment?: IToken[];
};

export interface Diagnostic_directiveCstNode extends CstNode {
  name: 'diagnostic_directive';
  children: Diagnostic_directiveCstChildren;
}

export type Diagnostic_directiveCstChildren = {
  Diagnostic: IToken[];
  LPar: IToken[];
  SeverityControlName: IToken[];
  Comma: IToken[];
  diagnostic_name: IToken[];
  Dot?: IToken[];
  diagnostic_dot_name?: IToken[];
  optional_comma?: IToken[];
  RPar: IToken[];
  Semi: IToken[];
};

export interface Enable_directiveCstNode extends CstNode {
  name: 'enable_directive';
  children: Enable_directiveCstChildren;
}

export type Enable_directiveCstChildren = {
  Enable: IToken[];
  extension_name: IToken[];
  Comma?: IToken[];
  optional_comma?: IToken[];
  Semi: IToken[];
};

export interface Requires_directiveCstNode extends CstNode {
  name: 'requires_directive';
  children: Requires_directiveCstChildren;
}

export type Requires_directiveCstChildren = {
  Requires: IToken[];
  extension_name: IToken[];
  Comma?: IToken[];
  optional_comma?: IToken[];
  Semi: IToken[];
};

export interface Global_declarationCstNode extends CstNode {
  name: 'global_declaration';
  children: Global_declarationCstChildren;
}

export type Global_declarationCstChildren = {
  Comment?: IToken[];
  Semi?: IToken[];
  const_declaration?: Const_declarationCstNode[];
  type_alias_declaration?: Type_alias_declarationCstNode[];
  const_assert_statement?: Const_assert_statementCstNode[];
  struct_declaration?: Struct_declarationCstNode[];
  override_declarations?: Override_declarationsCstNode[];
  function_declaration?: Function_declarationCstNode[];
  global_variable_declaration?: Global_variable_declarationCstNode[];
};

export interface Template_listCstNode extends CstNode {
  name: 'template_list';
  children: Template_listCstChildren;
}

export type Template_listCstChildren = {
  TemplateArgsStart: IToken[];
  expression: ExpressionCstNode[];
  Comma?: IToken[];
  optional_comma?: IToken[];
  TemplateArgsEnd: IToken[];
};

export interface Type_definitionCstNode extends CstNode {
  name: 'type_definition';
  children: Type_definitionCstChildren;
}

export type Type_definitionCstChildren = {
  Colon: IToken[];
  type: IToken[];
  template_list?: Template_listCstNode[];
};

export interface AttributeCstNode extends CstNode {
  name: 'attribute';
  children: AttributeCstChildren;
}

export type AttributeCstChildren = {
  attribute_single_arg?: Attribute_single_argCstNode[];
  attribute_no_args?: Attribute_no_argsCstNode[];
  diagnostic_attribute?: Diagnostic_attributeCstNode[];
  interpolate_attribute?: Interpolate_attributeCstNode[];
  workgroup_size_attribute?: Workgroup_size_attributeCstNode[];
};

export interface Diagnostic_attributeCstNode extends CstNode {
  name: 'diagnostic_attribute';
  children: Diagnostic_attributeCstChildren;
}

export type Diagnostic_attributeCstChildren = {
  DiagnosticAttr: IToken[];
  LPar: IToken[];
  SeverityControlName: IToken[];
  Comma: IToken[];
  diagnostic_name: IToken[];
  Dot?: IToken[];
  diagnostic_dot_name?: IToken[];
  optional_comma?: IToken[];
  RPar: IToken[];
};

export interface Interpolate_attributeCstNode extends CstNode {
  name: 'interpolate_attribute';
  children: Interpolate_attributeCstChildren;
}

export type Interpolate_attributeCstChildren = {
  InterpolateAttr: IToken[];
  LPar: IToken[];
  type: ExpressionCstNode[];
  Comma?: IToken[];
  sampling?: ExpressionCstNode[];
  optional_comma?: IToken[];
  RPar: IToken[];
};

export interface Workgroup_size_attributeCstNode extends CstNode {
  name: 'workgroup_size_attribute';
  children: Workgroup_size_attributeCstChildren;
}

export type Workgroup_size_attributeCstChildren = {
  WorkgroupSizeAttr: IToken[];
  LPar: IToken[];
  x: ExpressionCstNode[];
  Comma?: IToken[];
  y?: ExpressionCstNode[];
  z?: ExpressionCstNode[];
  optional_comma?: IToken[];
  RPar: IToken[];
};

export interface Attribute_no_argsCstNode extends CstNode {
  name: 'attribute_no_args';
  children: Attribute_no_argsCstChildren;
}

export type Attribute_no_argsCstChildren = {
  AttributeNoParam: IToken[];
};

export interface Attribute_single_argCstNode extends CstNode {
  name: 'attribute_single_arg';
  children: Attribute_single_argCstChildren;
}

export type Attribute_single_argCstChildren = {
  AttributeSingleParam: IToken[];
  LPar: IToken[];
  expression: ExpressionCstNode[];
  optional_comma?: IToken[];
  RPar: IToken[];
};

export interface Struct_declarationCstNode extends CstNode {
  name: 'struct_declaration';
  children: Struct_declarationCstChildren;
}

export type Struct_declarationCstChildren = {
  Struct: IToken[];
  name: IToken[];
  LCurly: IToken[];
  struct_member: Struct_memberCstNode[];
  Comma?: IToken[];
  optional_comma?: IToken[];
  RCurly: IToken[];
};

export interface Struct_memberCstNode extends CstNode {
  name: 'struct_member';
  children: Struct_memberCstChildren;
}

export type Struct_memberCstChildren = {
  attribute?: AttributeCstNode[];
  name: IToken[];
  type_definition: Type_definitionCstNode[];
};

export interface Type_alias_declarationCstNode extends CstNode {
  name: 'type_alias_declaration';
  children: Type_alias_declarationCstChildren;
}

export type Type_alias_declarationCstChildren = {
  Alias: IToken[];
  name: IToken[];
  Eq: IToken[];
  type: IToken[];
  template_list?: Template_listCstNode[];
  Semi: IToken[];
};

export interface Variable_or_value_statementCstNode extends CstNode {
  name: 'variable_or_value_statement';
  children: Variable_or_value_statementCstChildren;
}

export type Variable_or_value_statementCstChildren = {
  variable_declaration?: Variable_declarationCstNode[];
  let_declaration?: Let_declarationCstNode[];
  const_declaration?: Const_declarationCstNode[];
};

export interface Variable_declarationCstNode extends CstNode {
  name: 'variable_declaration';
  children: Variable_declarationCstChildren;
}

export type Variable_declarationCstChildren = {
  Var: IToken[];
  template_list?: Template_listCstNode[];
  name: IToken[];
  type_definition?: Type_definitionCstNode[];
  Eq?: IToken[];
  expression?: ExpressionCstNode[];
};

export interface Let_declarationCstNode extends CstNode {
  name: 'let_declaration';
  children: Let_declarationCstChildren;
}

export type Let_declarationCstChildren = {
  Let: IToken[];
  name: IToken[];
  type_definition?: Type_definitionCstNode[];
  Eq: IToken[];
  expression: ExpressionCstNode[];
};

export interface Const_declarationCstNode extends CstNode {
  name: 'const_declaration';
  children: Const_declarationCstChildren;
}

export type Const_declarationCstChildren = {
  Const: IToken[];
  name: IToken[];
  type_definition?: Type_definitionCstNode[];
  Eq: IToken[];
  expression: ExpressionCstNode[];
};

export interface Override_declarationsCstNode extends CstNode {
  name: 'override_declarations';
  children: Override_declarationsCstChildren;
}

export type Override_declarationsCstChildren = {
  attribute?: AttributeCstNode[];
  Override: IToken[];
  name: IToken[];
  type_definition?: Type_definitionCstNode[];
  Eq?: IToken[];
  expression?: ExpressionCstNode[];
  Semi: IToken[];
};

export interface Global_variable_declarationCstNode extends CstNode {
  name: 'global_variable_declaration';
  children: Global_variable_declarationCstChildren;
}

export type Global_variable_declarationCstChildren = {
  attribute?: AttributeCstNode[];
  variable_declaration: Variable_declarationCstNode[];
  Semi: IToken[];
};

export interface ExpressionCstNode extends CstNode {
  name: 'expression';
  children: ExpressionCstChildren;
}

export type ExpressionCstChildren = {
  short_circuit_expression: Short_circuit_expressionCstNode[];
};

export interface Paren_expressionCstNode extends CstNode {
  name: 'paren_expression';
  children: Paren_expressionCstChildren;
}

export type Paren_expressionCstChildren = {
  LPar: IToken[];
  expression: ExpressionCstNode[];
  RPar: IToken[];
};

export interface Ident_accessCstNode extends CstNode {
  name: 'ident_access';
  children: Ident_accessCstChildren;
}

export type Ident_accessCstChildren = {
  identifier: IToken[];
  template_list?: Template_listCstNode[];
  component_or_swizzle_specifier?: Component_or_swizzle_specifierCstNode[];
};

export interface Component_or_swizzle_specifierCstNode extends CstNode {
  name: 'component_or_swizzle_specifier';
  children: Component_or_swizzle_specifierCstChildren;
}

export type Component_or_swizzle_specifierCstChildren = {
  LBrack?: IToken[];
  expression?: ExpressionCstNode[];
  RBrack?: IToken[];
  Dot?: IToken[];
  member_ident?: IToken[];
  SwizzleName?: IToken[];
  component_or_swizzle_specifier?: Component_or_swizzle_specifierCstNode[];
};

export interface Call_phraseCstNode extends CstNode {
  name: 'call_phrase';
  children: Call_phraseCstChildren;
}

export type Call_phraseCstChildren = {
  identifier: IToken[];
  template_list?: Template_listCstNode[];
  LPar: IToken[];
  expression: ExpressionCstNode[];
  Comma?: IToken[];
  optional_comma?: IToken[];
  RPar: IToken[];
};

export interface Short_circuit_expressionCstNode extends CstNode {
  name: 'short_circuit_expression';
  children: Short_circuit_expressionCstChildren;
}

export type Short_circuit_expressionCstChildren = {
  relational_expression: Relational_expressionCstNode[];
  LogicOperator?: IToken[];
};

export interface Relational_expressionCstNode extends CstNode {
  name: 'relational_expression';
  children: Relational_expressionCstChildren;
}

export type Relational_expressionCstChildren = {
  shift_expression: Shift_expressionCstNode[];
  RelationalOperator?: IToken[];
};

export interface Shift_expressionCstNode extends CstNode {
  name: 'shift_expression';
  children: Shift_expressionCstChildren;
}

export type Shift_expressionCstChildren = {
  additive_expression: Additive_expressionCstNode[];
  ShiftOperator?: IToken[];
};

export interface Additive_expressionCstNode extends CstNode {
  name: 'additive_expression';
  children: Additive_expressionCstChildren;
}

export type Additive_expressionCstChildren = {
  multiplicative_expression: Multiplicative_expressionCstNode[];
  AdditiveOperator?: IToken[];
};

export interface Multiplicative_expressionCstNode extends CstNode {
  name: 'multiplicative_expression';
  children: Multiplicative_expressionCstChildren;
}

export type Multiplicative_expressionCstChildren = {
  binary_expression: Binary_expressionCstNode[];
  MultiplicativeOperator?: IToken[];
};

export interface Binary_expressionCstNode extends CstNode {
  name: 'binary_expression';
  children: Binary_expressionCstChildren;
}

export type Binary_expressionCstChildren = {
  unary_expression: Unary_expressionCstNode[];
  BinaryOperator?: IToken[];
};

export interface Unary_expressionCstNode extends CstNode {
  name: 'unary_expression';
  children: Unary_expressionCstChildren;
}

export type Unary_expressionCstChildren = {
  UnaryOperator?: IToken[];
  atom: AtomCstNode[];
};

export interface AtomCstNode extends CstNode {
  name: 'atom';
  children: AtomCstChildren;
}

export type AtomCstChildren = {
  literal?: LiteralCstNode[];
  paren_expression?: Paren_expressionCstNode[];
  call_phrase?: Call_phraseCstNode[];
  ident_access?: Ident_accessCstNode[];
};

export interface Lhs_expressionCstNode extends CstNode {
  name: 'lhs_expression';
  children: Lhs_expressionCstChildren;
}

export type Lhs_expressionCstChildren = {
  lhs_expression_ident?: Lhs_expression_identCstNode[];
  lhs_expression_parentesis?: Lhs_expression_parentesisCstNode[];
  Mult?: IToken[];
  lhs_expression?: Lhs_expressionCstNode[];
  Amp?: IToken[];
};

export interface Lhs_expression_identCstNode extends CstNode {
  name: 'lhs_expression_ident';
  children: Lhs_expression_identCstChildren;
}

export type Lhs_expression_identCstChildren = {
  name: IToken[];
  component_or_swizzle_specifier?: Component_or_swizzle_specifierCstNode[];
};

export interface Lhs_expression_parentesisCstNode extends CstNode {
  name: 'lhs_expression_parentesis';
  children: Lhs_expression_parentesisCstChildren;
}

export type Lhs_expression_parentesisCstChildren = {
  LPar: IToken[];
  lhs_expression: Lhs_expressionCstNode[];
  RPar: IToken[];
  component_or_swizzle_specifier?: Component_or_swizzle_specifierCstNode[];
};

export interface Compound_statementCstNode extends CstNode {
  name: 'compound_statement';
  children: Compound_statementCstChildren;
}

export type Compound_statementCstChildren = {
  attribute?: AttributeCstNode[];
  LCurly: IToken[];
  statement?: StatementCstNode[];
  RCurly: IToken[];
};

export interface Assignment_statementCstNode extends CstNode {
  name: 'assignment_statement';
  children: Assignment_statementCstChildren;
}

export type Assignment_statementCstChildren = {
  lhs_expression: Lhs_expressionCstNode[];
  operator?: IToken[];
  expression?: ExpressionCstNode[];
  PlusPlus?: IToken[];
  MinusMinus?: IToken[];
};

export interface Phony_assignmentCstNode extends CstNode {
  name: 'phony_assignment';
  children: Phony_assignmentCstChildren;
}

export type Phony_assignmentCstChildren = {
  Unserscore: IToken[];
  Eq: IToken[];
  phony_value: ExpressionCstNode[];
};

export interface If_statementCstNode extends CstNode {
  name: 'if_statement';
  children: If_statementCstChildren;
}

export type If_statementCstChildren = {
  attribute?: AttributeCstNode[];
  if_clause: If_clauseCstNode[];
  else_if_clause?: Else_if_clauseCstNode[];
  else_clause?: Else_clauseCstNode[];
};

export interface If_clauseCstNode extends CstNode {
  name: 'if_clause';
  children: If_clauseCstChildren;
}

export type If_clauseCstChildren = {
  If: IToken[];
  expression: ExpressionCstNode[];
  compound_statement: Compound_statementCstNode[];
};

export interface Else_if_clauseCstNode extends CstNode {
  name: 'else_if_clause';
  children: Else_if_clauseCstChildren;
}

export type Else_if_clauseCstChildren = {
  Else: IToken[];
  If: IToken[];
  expression: ExpressionCstNode[];
  compound_statement: Compound_statementCstNode[];
};

export interface Else_clauseCstNode extends CstNode {
  name: 'else_clause';
  children: Else_clauseCstChildren;
}

export type Else_clauseCstChildren = {
  Else: IToken[];
  compound_statement: Compound_statementCstNode[];
};

export interface Switch_statementCstNode extends CstNode {
  name: 'switch_statement';
  children: Switch_statementCstChildren;
}

export type Switch_statementCstChildren = {
  attribute?: AttributeCstNode[];
  Switch: IToken[];
  expression: ExpressionCstNode[];
  body_attribute?: AttributeCstNode[];
  LCurly: IToken[];
  switch_clause?: Switch_clauseCstNode[];
  RCurly: IToken[];
};

export interface Switch_clauseCstNode extends CstNode {
  name: 'switch_clause';
  children: Switch_clauseCstChildren;
}

export type Switch_clauseCstChildren = {
  case_clause?: Case_clauseCstNode[];
  default_alone_clause?: Default_alone_clauseCstNode[];
};

export interface Case_clauseCstNode extends CstNode {
  name: 'case_clause';
  children: Case_clauseCstChildren;
}

export type Case_clauseCstChildren = {
  Case: IToken[];
  case_selector: Case_selectorCstNode[];
  Comma?: IToken[];
  optional_comma?: IToken[];
  Colon?: IToken[];
  compound_statement: Compound_statementCstNode[];
};

export interface Default_alone_clauseCstNode extends CstNode {
  name: 'default_alone_clause';
  children: Default_alone_clauseCstChildren;
}

export type Default_alone_clauseCstChildren = {
  Default: IToken[];
  Colon?: IToken[];
  compound_statement: Compound_statementCstNode[];
};

export interface Case_selectorCstNode extends CstNode {
  name: 'case_selector';
  children: Case_selectorCstChildren;
}

export type Case_selectorCstChildren = {
  Default?: IToken[];
  expression?: ExpressionCstNode[];
};

export interface Loop_statementCstNode extends CstNode {
  name: 'loop_statement';
  children: Loop_statementCstChildren;
}

export type Loop_statementCstChildren = {
  attribute?: AttributeCstNode[];
  Loop: IToken[];
  body_attribute?: AttributeCstNode[];
  LCurly: IToken[];
  statement?: StatementCstNode[];
  continuing_statement?: Continuing_statementCstNode[];
  RCurly: IToken[];
};

export interface For_statementCstNode extends CstNode {
  name: 'for_statement';
  children: For_statementCstChildren;
}

export type For_statementCstChildren = {
  attribute?: AttributeCstNode[];
  For: IToken[];
  LPar: IToken[];
  for_init?: For_initCstNode[];
  Semi: IToken[];
  for_condition?: ExpressionCstNode[];
  for_update?: For_updateCstNode[];
  RPar: IToken[];
  compound_statement: Compound_statementCstNode[];
};

export interface For_initCstNode extends CstNode {
  name: 'for_init';
  children: For_initCstChildren;
}

export type For_initCstChildren = {
  variable_or_value_statement?: Variable_or_value_statementCstNode[];
  variable_updating_statement?: Variable_updating_statementCstNode[];
  call_phrase?: Call_phraseCstNode[];
};

export interface For_updateCstNode extends CstNode {
  name: 'for_update';
  children: For_updateCstChildren;
}

export type For_updateCstChildren = {
  variable_updating_statement?: Variable_updating_statementCstNode[];
  call_phrase?: Call_phraseCstNode[];
};

export interface While_statementCstNode extends CstNode {
  name: 'while_statement';
  children: While_statementCstChildren;
}

export type While_statementCstChildren = {
  attribute?: AttributeCstNode[];
  While: IToken[];
  expression: ExpressionCstNode[];
  compound_statement: Compound_statementCstNode[];
};

export interface Break_statementCstNode extends CstNode {
  name: 'break_statement';
  children: Break_statementCstChildren;
}

export type Break_statementCstChildren = {
  break: IToken[];
  Semi: IToken[];
};

export interface Break_if_statementCstNode extends CstNode {
  name: 'break_if_statement';
  children: Break_if_statementCstChildren;
}

export type Break_if_statementCstChildren = {
  break: IToken[];
  If: IToken[];
  expression: ExpressionCstNode[];
  Semi: IToken[];
};

export interface Continue_statementCstNode extends CstNode {
  name: 'continue_statement';
  children: Continue_statementCstChildren;
}

export type Continue_statementCstChildren = {
  Continue: IToken[];
  Semi: IToken[];
};

export interface Discard_statementCstNode extends CstNode {
  name: 'discard_statement';
  children: Discard_statementCstChildren;
}

export type Discard_statementCstChildren = {
  Discard: IToken[];
  Semi: IToken[];
};

export interface Continuing_statementCstNode extends CstNode {
  name: 'continuing_statement';
  children: Continuing_statementCstChildren;
}

export type Continuing_statementCstChildren = {
  Continuing: IToken[];
  attribute?: AttributeCstNode[];
  LCurly: IToken[];
  statement?: StatementCstNode[];
  break_if_statement?: Break_if_statementCstNode[];
  RCurly: IToken[];
};

export interface Return_statementCstNode extends CstNode {
  name: 'return_statement';
  children: Return_statementCstChildren;
}

export type Return_statementCstChildren = {
  Return: IToken[];
  expression?: ExpressionCstNode[];
  Semi: IToken[];
};

export interface Const_assert_statementCstNode extends CstNode {
  name: 'const_assert_statement';
  children: Const_assert_statementCstChildren;
}

export type Const_assert_statementCstChildren = {
  ConstAssert: IToken[];
  expression: ExpressionCstNode[];
  Semi: IToken[];
};

export interface StatementCstNode extends CstNode {
  name: 'statement';
  children: StatementCstChildren;
}

export type StatementCstChildren = {
  Semi?: IToken[];
  Comment?: IToken[];
  return_statement?: Return_statementCstNode[];
  break_statement?: Break_statementCstNode[];
  continue_statement?: Continue_statementCstNode[];
  discard_statement?: Discard_statementCstNode[];
  const_assert_statement?: Const_assert_statementCstNode[];
  if_statement?: If_statementCstNode[];
  switch_statement?: Switch_statementCstNode[];
  loop_statement?: Loop_statementCstNode[];
  for_statement?: For_statementCstNode[];
  while_statement?: While_statementCstNode[];
  call_phrase?: Call_phraseCstNode[];
  variable_or_value_statement?: Variable_or_value_statementCstNode[];
  variable_updating_statement?: Variable_updating_statementCstNode[];
  compound_statement?: Compound_statementCstNode[];
};

export interface Variable_updating_statementCstNode extends CstNode {
  name: 'variable_updating_statement';
  children: Variable_updating_statementCstChildren;
}

export type Variable_updating_statementCstChildren = {
  phony_assignment?: Phony_assignmentCstNode[];
  assignment_statement?: Assignment_statementCstNode[];
};

export interface Function_declarationCstNode extends CstNode {
  name: 'function_declaration';
  children: Function_declarationCstChildren;
}

export type Function_declarationCstChildren = {
  function_attributes?: AttributeCstNode[];
  Fn: IToken[];
  name: IToken[];
  LPar: IToken[];
  param?: ParamCstNode[];
  Comma?: IToken[];
  optional_comma?: IToken[];
  RPar: IToken[];
  Arrow?: IToken[];
  return_attributes?: AttributeCstNode[];
  return_type?: IToken[];
  template_list?: Template_listCstNode[];
  compound_statement: Compound_statementCstNode[];
};

export interface ParamCstNode extends CstNode {
  name: 'param';
  children: ParamCstChildren;
}

export type ParamCstChildren = {
  attribute?: AttributeCstNode[];
  name: IToken[];
  type_definition?: Type_definitionCstNode[];
};

export interface Int_literalCstNode extends CstNode {
  name: 'int_literal';
  children: Int_literalCstChildren;
}

export type Int_literalCstChildren = {
  DecimalIntLiteral?: IToken[];
  HexIntLiteral?: IToken[];
};

export interface Float_literalCstNode extends CstNode {
  name: 'float_literal';
  children: Float_literalCstChildren;
}

export type Float_literalCstChildren = {
  DecimalFloatLiteral?: IToken[];
  HexFloatLiteral?: IToken[];
};

export interface LiteralCstNode extends CstNode {
  name: 'literal';
  children: LiteralCstChildren;
}

export type LiteralCstChildren = {
  int_literal?: Int_literalCstNode[];
  float_literal?: Float_literalCstNode[];
  BooleanLiteral?: IToken[];
};

export interface ICstNodeVisitor<IN, OUT> extends ICstVisitor<IN, OUT> {
  programm(children: ProgrammCstChildren, param?: IN): OUT;
  global_directive(children: Global_directiveCstChildren, param?: IN): OUT;
  diagnostic_directive(
    children: Diagnostic_directiveCstChildren,
    param?: IN
  ): OUT;
  enable_directive(children: Enable_directiveCstChildren, param?: IN): OUT;
  requires_directive(children: Requires_directiveCstChildren, param?: IN): OUT;
  global_declaration(children: Global_declarationCstChildren, param?: IN): OUT;
  template_list(children: Template_listCstChildren, param?: IN): OUT;
  type_definition(children: Type_definitionCstChildren, param?: IN): OUT;
  attribute(children: AttributeCstChildren, param?: IN): OUT;
  diagnostic_attribute(
    children: Diagnostic_attributeCstChildren,
    param?: IN
  ): OUT;
  interpolate_attribute(
    children: Interpolate_attributeCstChildren,
    param?: IN
  ): OUT;
  workgroup_size_attribute(
    children: Workgroup_size_attributeCstChildren,
    param?: IN
  ): OUT;
  attribute_no_args(children: Attribute_no_argsCstChildren, param?: IN): OUT;
  attribute_single_arg(
    children: Attribute_single_argCstChildren,
    param?: IN
  ): OUT;
  struct_declaration(children: Struct_declarationCstChildren, param?: IN): OUT;
  struct_member(children: Struct_memberCstChildren, param?: IN): OUT;
  type_alias_declaration(
    children: Type_alias_declarationCstChildren,
    param?: IN
  ): OUT;
  variable_or_value_statement(
    children: Variable_or_value_statementCstChildren,
    param?: IN
  ): OUT;
  variable_declaration(
    children: Variable_declarationCstChildren,
    param?: IN
  ): OUT;
  let_declaration(children: Let_declarationCstChildren, param?: IN): OUT;
  const_declaration(children: Const_declarationCstChildren, param?: IN): OUT;
  override_declarations(
    children: Override_declarationsCstChildren,
    param?: IN
  ): OUT;
  global_variable_declaration(
    children: Global_variable_declarationCstChildren,
    param?: IN
  ): OUT;
  expression(children: ExpressionCstChildren, param?: IN): OUT;
  paren_expression(children: Paren_expressionCstChildren, param?: IN): OUT;
  ident_access(children: Ident_accessCstChildren, param?: IN): OUT;
  component_or_swizzle_specifier(
    children: Component_or_swizzle_specifierCstChildren,
    param?: IN
  ): OUT;
  call_phrase(children: Call_phraseCstChildren, param?: IN): OUT;
  short_circuit_expression(
    children: Short_circuit_expressionCstChildren,
    param?: IN
  ): OUT;
  relational_expression(
    children: Relational_expressionCstChildren,
    param?: IN
  ): OUT;
  shift_expression(children: Shift_expressionCstChildren, param?: IN): OUT;
  additive_expression(
    children: Additive_expressionCstChildren,
    param?: IN
  ): OUT;
  multiplicative_expression(
    children: Multiplicative_expressionCstChildren,
    param?: IN
  ): OUT;
  binary_expression(children: Binary_expressionCstChildren, param?: IN): OUT;
  unary_expression(children: Unary_expressionCstChildren, param?: IN): OUT;
  atom(children: AtomCstChildren, param?: IN): OUT;
  lhs_expression(children: Lhs_expressionCstChildren, param?: IN): OUT;
  lhs_expression_ident(
    children: Lhs_expression_identCstChildren,
    param?: IN
  ): OUT;
  lhs_expression_parentesis(
    children: Lhs_expression_parentesisCstChildren,
    param?: IN
  ): OUT;
  compound_statement(children: Compound_statementCstChildren, param?: IN): OUT;
  assignment_statement(
    children: Assignment_statementCstChildren,
    param?: IN
  ): OUT;
  phony_assignment(children: Phony_assignmentCstChildren, param?: IN): OUT;
  if_statement(children: If_statementCstChildren, param?: IN): OUT;
  if_clause(children: If_clauseCstChildren, param?: IN): OUT;
  else_if_clause(children: Else_if_clauseCstChildren, param?: IN): OUT;
  else_clause(children: Else_clauseCstChildren, param?: IN): OUT;
  switch_statement(children: Switch_statementCstChildren, param?: IN): OUT;
  switch_clause(children: Switch_clauseCstChildren, param?: IN): OUT;
  case_clause(children: Case_clauseCstChildren, param?: IN): OUT;
  default_alone_clause(
    children: Default_alone_clauseCstChildren,
    param?: IN
  ): OUT;
  case_selector(children: Case_selectorCstChildren, param?: IN): OUT;
  loop_statement(children: Loop_statementCstChildren, param?: IN): OUT;
  for_statement(children: For_statementCstChildren, param?: IN): OUT;
  for_init(children: For_initCstChildren, param?: IN): OUT;
  for_update(children: For_updateCstChildren, param?: IN): OUT;
  while_statement(children: While_statementCstChildren, param?: IN): OUT;
  break_statement(children: Break_statementCstChildren, param?: IN): OUT;
  break_if_statement(children: Break_if_statementCstChildren, param?: IN): OUT;
  continue_statement(children: Continue_statementCstChildren, param?: IN): OUT;
  discard_statement(children: Discard_statementCstChildren, param?: IN): OUT;
  continuing_statement(
    children: Continuing_statementCstChildren,
    param?: IN
  ): OUT;
  return_statement(children: Return_statementCstChildren, param?: IN): OUT;
  const_assert_statement(
    children: Const_assert_statementCstChildren,
    param?: IN
  ): OUT;
  statement(children: StatementCstChildren, param?: IN): OUT;
  variable_updating_statement(
    children: Variable_updating_statementCstChildren,
    param?: IN
  ): OUT;
  function_declaration(
    children: Function_declarationCstChildren,
    param?: IN
  ): OUT;
  param(children: ParamCstChildren, param?: IN): OUT;
  int_literal(children: Int_literalCstChildren, param?: IN): OUT;
  float_literal(children: Float_literalCstChildren, param?: IN): OUT;
  literal(children: LiteralCstChildren, param?: IN): OUT;
}
