import { WgslLexer, disambiguateTemplateLists } from "./lexer";
import { parser } from "./parser";

// const ast = new WSDLAstVisitor();
// console.log(generateCstDts(parser.getGAstProductions()));

export function parse(text: string) {
  const result = WgslLexer.tokenize(text);
  const tokens = disambiguateTemplateLists(result.tokens);
  // setting a new input will RESET the parser instance's state.
  parser.input = tokens;

  const value = parser.programm();

  return {
    // ast: ast.visit(value),
    value,
    tokens,
    lexerErrors: result.errors,
    parseErrors: parser.errors,
  };
}
