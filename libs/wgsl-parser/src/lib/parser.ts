import { CstParser, TokenVocabulary } from 'chevrotain';
import * as t from './tokens';
import { WgslAstNode, WgslAstParams } from './ast.types';

export class WgslParser extends CstParser {
  constructor(tokens: TokenVocabulary = t.allTokens) {
    super(tokens, {
      recoveryEnabled: true,
      nodeLocationTracking: 'onlyOffset',
    });
    this.performSelfAnalysis();
  }

  programm = this.RULE('programm', () => {
    this.MANY(() => this.SUBRULE(this.global_directive));
    this.MANY2(() => this.SUBRULE(this.global_declaration));
  });

  global_directive = this.RULE('global_directive', () => {
    this.OR([
      { ALT: () => this.SUBRULE(this.diagnostic_directive) },
      { ALT: () => this.SUBRULE(this.enable_directive) },
      { ALT: () => this.SUBRULE(this.requires_directive) },
      { ALT: () => this.CONSUME(t.Comment) },
    ]);
  });

  diagnostic_directive = this.RULE('diagnostic_directive', () => {
    this.CONSUME(t.Diagnostic);
    this.CONSUME(t.LPar);
    this.CONSUME(t.SeverityControlName);
    this.CONSUME(t.Comma);
    this.CONSUME(t.IdentPatternToken, { LABEL: 'diagnostic_name' });
    this.OPTION(() => {
      this.CONSUME(t.Dot);
      this.CONSUME2(t.IdentPatternToken, { LABEL: 'diagnostic_dot_name' });
    });
    this.OPTION2(() => this.CONSUME2(t.Comma, { LABEL: 'optional_comma' }));
    this.CONSUME(t.RPar);
    this.CONSUME(t.Semi);
  });

  enable_directive = this.RULE('enable_directive', () => {
    this.CONSUME(t.Enable);
    this.CONSUME(t.IdentPatternToken, { LABEL: 'extension_name' });
    this.MANY(() => {
      this.CONSUME(t.Comma);
      this.CONSUME2(t.IdentPatternToken, { LABEL: 'extension_name' });
    });
    this.OPTION(() => this.CONSUME2(t.Comma, { LABEL: 'optional_comma' }));
    this.CONSUME(t.Semi);
  });

  requires_directive = this.RULE('requires_directive', () => {
    this.CONSUME(t.Requires);
    this.CONSUME(t.IdentPatternToken, { LABEL: 'extension_name' });
    this.MANY(() => {
      this.CONSUME(t.Comma);
      this.CONSUME2(t.IdentPatternToken, { LABEL: 'extension_name' });
    });
    this.OPTION(() => this.CONSUME2(t.Comma, { LABEL: 'optional_comma' }));
    this.CONSUME(t.Semi);
  });

  global_declaration = this.RULE('global_declaration', () => {
    this.OR([
      { ALT: () => this.CONSUME(t.Comment) },
      { ALT: () => this.CONSUME(t.Semi) },
      {
        ALT: () => {
          this.SUBRULE(this.const_declaration);
          this.CONSUME2(t.Semi);
        },
      },
      { ALT: () => this.SUBRULE(this.type_alias_declaration) },
      { ALT: () => this.SUBRULE(this.const_assert_statement) },
      { ALT: () => this.SUBRULE(this.struct_declaration) },
      {
        GATE: this.BACKTRACK(this.override_declarations),
        ALT: () => this.SUBRULE(this.override_declarations),
      },
      {
        GATE: this.BACKTRACK(this.function_declaration),
        ALT: () => this.SUBRULE(this.function_declaration),
      },
      {
        GATE: this.BACKTRACK(this.global_variable_declaration),
        ALT: () => this.SUBRULE(this.global_variable_declaration),
      },
    ]);
  });

  template_list = this.RULE('template_list', () => {
    this.CONSUME(t.TemplateArgsStart);
    this.SUBRULE(this.expression);
    this.MANY(() => {
      this.CONSUME(t.Comma);
      this.SUBRULE2(this.expression);
    });
    this.OPTION(() => this.CONSUME2(t.Comma, { LABEL: 'optional_comma' }));
    this.CONSUME(t.TemplateArgsEnd);
  });

  type_definition = this.RULE('type_definition', () => {
    this.CONSUME(t.Colon);
    this.CONSUME(t.IdentPatternToken, { LABEL: 'type' });
    this.OPTION(() => this.SUBRULE(this.template_list));
  });

  attribute = this.RULE('attribute', () => {
    this.OR([
      { ALT: () => this.SUBRULE(this.attribute_single_arg) },
      { ALT: () => this.SUBRULE(this.attribute_no_args) },
      { ALT: () => this.SUBRULE(this.diagnostic_attribute) },
      { ALT: () => this.SUBRULE(this.interpolate_attribute) },
      { ALT: () => this.SUBRULE(this.workgroup_size_attribute) },
    ]);
  });

  diagnostic_attribute = this.RULE('diagnostic_attribute', () => {
    this.CONSUME(t.DiagnosticAttr);
    this.CONSUME(t.LPar);
    this.CONSUME(t.SeverityControlName);
    this.CONSUME(t.Comma);
    this.CONSUME(t.IdentPatternToken, { LABEL: 'diagnostic_name' });
    this.OPTION(() => {
      this.CONSUME(t.Dot);
      this.CONSUME2(t.IdentPatternToken, { LABEL: 'diagnostic_dot_name' });
    });
    this.OPTION2(() => this.CONSUME2(t.Comma, { LABEL: 'optional_comma' }));
    this.CONSUME(t.RPar);
  });

  interpolate_attribute = this.RULE('interpolate_attribute', () => {
    this.CONSUME(t.InterpolateAttr);
    this.CONSUME(t.LPar);
    this.SUBRULE(this.expression, { LABEL: 'type' });
    this.OPTION(() => {
      this.CONSUME(t.Comma);
      this.SUBRULE2(this.expression, { LABEL: 'sampling' });
    });
    this.OPTION2(() => this.CONSUME2(t.Comma, { LABEL: 'optional_comma' }));
    this.CONSUME(t.RPar);
  });

  workgroup_size_attribute = this.RULE('workgroup_size_attribute', () => {
    this.CONSUME(t.WorkgroupSizeAttr);
    this.CONSUME2(t.LPar);
    this.SUBRULE3(this.expression, { LABEL: 'x' });
    this.OPTION2(() => {
      this.CONSUME2(t.Comma);
      this.SUBRULE4(this.expression, { LABEL: 'y' });
    });
    this.OPTION3(() => {
      this.CONSUME3(t.Comma);
      this.SUBRULE5(this.expression, { LABEL: 'z' });
    });
    this.OPTION4(() => this.CONSUME4(t.Comma, { LABEL: 'optional_comma' }));
    this.CONSUME(t.RPar);
  });

  attribute_no_args = this.RULE('attribute_no_args', () => {
    this.CONSUME(t.AttributeNoParam);
  });

  attribute_single_arg = this.RULE('attribute_single_arg', () => {
    this.CONSUME(t.AttributeSingleParam);
    this.CONSUME(t.LPar);
    this.SUBRULE(this.expression);
    this.OPTION(() => this.CONSUME(t.Comma, { LABEL: 'optional_comma' }));
    this.CONSUME(t.RPar);
  });

  struct_declaration = this.RULE('struct_declaration', () => {
    this.CONSUME(t.Struct);
    this.CONSUME(t.IdentPatternToken, { LABEL: 'name' });
    this.CONSUME(t.LCurly);
    this.SUBRULE(this.struct_member);
    this.MANY(() => {
      this.CONSUME(t.Comma);
      this.SUBRULE2(this.struct_member);
    });
    this.OPTION(() => this.CONSUME2(t.Comma, { LABEL: 'optional_comma' }));
    this.CONSUME(t.RCurly);
  });

  struct_member = this.RULE('struct_member', () => {
    this.MANY(() => this.SUBRULE(this.attribute));
    this.CONSUME(t.IdentPatternToken, { LABEL: 'name' });
    this.SUBRULE(this.type_definition);
  });

  type_alias_declaration = this.RULE('type_alias_declaration', () => {
    this.CONSUME(t.Alias);
    this.CONSUME(t.IdentPatternToken, { LABEL: 'name' });
    this.CONSUME(t.Eq);
    this.CONSUME2(t.IdentPatternToken, { LABEL: 'type' });
    this.OPTION(() => this.SUBRULE(this.template_list));
    this.CONSUME(t.Semi);
  });

  variable_or_value_statement = this.RULE('variable_or_value_statement', () => {
    this.OR([
      { ALT: () => this.SUBRULE(this.variable_declaration) },
      { ALT: () => this.SUBRULE(this.let_declaration) },
      { ALT: () => this.SUBRULE(this.const_declaration) },
    ]);
  });

  variable_declaration = this.RULE('variable_declaration', () => {
    this.CONSUME(t.Var);
    this.OPTION(() => this.SUBRULE(this.template_list));
    this.CONSUME(t.IdentPatternToken, { LABEL: 'name' });
    this.OPTION2(() => this.SUBRULE(this.type_definition));
    this.OPTION3(() => {
      this.CONSUME(t.Eq);
      this.SUBRULE(this.expression);
    });
  });

  let_declaration = this.RULE('let_declaration', () => {
    this.CONSUME(t.Let);
    this.CONSUME(t.IdentPatternToken, { LABEL: 'name' });
    this.OPTION(() => this.SUBRULE(this.type_definition));
    this.CONSUME(t.Eq);
    this.SUBRULE(this.expression);
  });

  const_declaration = this.RULE('const_declaration', () => {
    this.CONSUME(t.Const);
    this.CONSUME(t.IdentPatternToken, { LABEL: 'name' });
    this.OPTION(() => this.SUBRULE(this.type_definition));
    this.CONSUME(t.Eq);
    this.SUBRULE(this.expression);
  });

  override_declarations = this.RULE('override_declarations', () => {
    this.MANY(() => this.SUBRULE(this.attribute));
    this.CONSUME(t.Override);
    this.CONSUME(t.IdentPatternToken, { LABEL: 'name' });
    this.OPTION(() => this.SUBRULE(this.type_definition));
    this.OPTION2(() => {
      this.CONSUME(t.Eq);
      this.SUBRULE(this.expression);
    });
    this.CONSUME(t.Semi);
  });

  global_variable_declaration = this.RULE('global_variable_declaration', () => {
    this.MANY(() => this.SUBRULE(this.attribute));
    this.SUBRULE(this.variable_declaration);
    this.CONSUME2(t.Semi);
  });

  expression = this.RULE('expression', () => {
    this.SUBRULE(this.short_circuit_expression);
  });

  paren_expression = this.RULE('paren_expression', () => {
    this.CONSUME(t.LPar);
    this.SUBRULE(this.expression);
    this.CONSUME(t.RPar);
  });

  ident_access = this.RULE('ident_access', () => {
    this.CONSUME(t.IdentPatternToken, { LABEL: 'identifier' });
    this.OPTION(() => this.SUBRULE(this.template_list));
    this.OPTION2(() => this.SUBRULE(this.component_or_swizzle_specifier));
  });

  component_or_swizzle_specifier = this.RULE(
    'component_or_swizzle_specifier',
    () => {
      this.OR([
        {
          ALT: () => {
            this.CONSUME(t.LBrack);
            this.SUBRULE(this.expression);
            this.CONSUME(t.RBrack);
          },
        },
        {
          ALT: () => {
            this.CONSUME(t.Dot);
            this.CONSUME(t.IdentPatternToken, { LABEL: 'member_ident' });
          },
        },
        {
          ALT: () => {
            this.CONSUME2(t.Dot);
            this.CONSUME(t.SwizzleName);
          },
        },
      ]);
      this.OPTION(() => this.SUBRULE(this.component_or_swizzle_specifier));
    }
  );

  call_phrase = this.RULE('call_phrase', () => {
    this.CONSUME(t.IdentPatternToken, { LABEL: 'identifier' });
    this.OPTION(() => this.SUBRULE(this.template_list));
    this.CONSUME(t.LPar);
    this.SUBRULE(this.expression);
    this.MANY(() => {
      this.CONSUME(t.Comma);
      this.SUBRULE2(this.expression);
    });
    this.OPTION2(() => this.CONSUME2(t.Comma, { LABEL: 'optional_comma' }));
    this.CONSUME(t.RPar);
  });

  short_circuit_expression = this.RULE('short_circuit_expression', () => {
    this.SUBRULE(this.relational_expression);
    this.MANY(() => {
      this.CONSUME(t.LogicOperator);
      this.SUBRULE2(this.relational_expression);
    });
  });

  relational_expression = this.RULE('relational_expression', () => {
    this.SUBRULE(this.shift_expression);
    this.MANY(() => {
      this.CONSUME(t.RelationalOperator);
      this.SUBRULE2(this.shift_expression);
    });
  });

  shift_expression = this.RULE('shift_expression', () => {
    this.SUBRULE(this.additive_expression);
    this.MANY(() => {
      this.CONSUME(t.ShiftOperator);
      this.SUBRULE2(this.additive_expression);
    });
  });

  additive_expression = this.RULE('additive_expression', () => {
    this.SUBRULE(this.multiplicative_expression);
    this.MANY(() => {
      this.CONSUME(t.AdditiveOperator);
      this.SUBRULE2(this.multiplicative_expression);
    });
  });

  multiplicative_expression = this.RULE('multiplicative_expression', () => {
    this.SUBRULE(this.binary_expression);
    this.MANY(() => {
      this.CONSUME(t.MultiplicativeOperator);
      this.SUBRULE2(this.binary_expression);
    });
  });

  binary_expression = this.RULE('binary_expression', () => {
    this.SUBRULE(this.unary_expression);
    this.MANY(() => {
      this.CONSUME(t.BinaryOperator);
      this.SUBRULE2(this.unary_expression);
    });
  });

  unary_expression = this.RULE('unary_expression', () => {
    this.OPTION(() => this.CONSUME(t.UnaryOperator));
    this.SUBRULE(this.atom);
  });

  atom = this.RULE('atom', () => {
    this.OR([
      { ALT: () => this.SUBRULE(this.literal) },
      { ALT: () => this.SUBRULE(this.paren_expression) },
      {
        GATE: this.BACKTRACK(this.call_phrase),
        ALT: () => this.SUBRULE(this.call_phrase),
      },
      { ALT: () => this.SUBRULE(this.ident_access) },
    ]);
  });

  lhs_expression = this.RULE('lhs_expression', () => {
    this.OR([
      { ALT: () => this.SUBRULE(this.lhs_expression_ident) },
      { ALT: () => this.SUBRULE(this.lhs_expression_parentesis) },
      {
        ALT: () => {
          this.CONSUME(t.Mult);
          this.SUBRULE2(this.lhs_expression);
        },
      },
      {
        ALT: () => {
          this.CONSUME(t.Amp);
          this.SUBRULE3(this.lhs_expression);
        },
      },
    ]);
  });

  lhs_expression_ident = this.RULE('lhs_expression_ident', () => {
    this.CONSUME(t.IdentPatternToken, { LABEL: 'name' });
    this.OPTION(() => this.SUBRULE(this.component_or_swizzle_specifier));
  });

  lhs_expression_parentesis = this.RULE('lhs_expression_parentesis', () => {
    this.CONSUME(t.LPar);
    this.SUBRULE(this.lhs_expression);
    this.CONSUME(t.RPar);
    this.OPTION(() => this.SUBRULE(this.component_or_swizzle_specifier));
  });

  compound_statement = this.RULE('compound_statement', () => {
    this.MANY(() => this.SUBRULE(this.attribute));
    this.CONSUME(t.LCurly);
    this.MANY2(() => this.SUBRULE(this.statement));
    this.CONSUME(t.RCurly);
  });

  assignment_statement = this.RULE('assignment_statement', () => {
    this.SUBRULE(this.lhs_expression);
    this.OR([
      {
        ALT: () => {
          this.CONSUME(t.AssignmentOperator, {
            LABEL: 'operator',
          });
          this.SUBRULE(this.expression);
        },
      },
      { ALT: () => this.CONSUME(t.PlusPlus) },
      { ALT: () => this.CONSUME(t.MinusMinus) },
    ]);
  });

  phony_assignment = this.RULE('phony_assignment', () => {
    this.CONSUME(t.Underscore);
    this.CONSUME2(t.Eq);
    this.SUBRULE(this.expression, { LABEL: 'phony_value' });
  });

  if_statement = this.RULE('if_statement', () => {
    this.MANY(() => this.SUBRULE(this.attribute));
    this.SUBRULE(this.if_clause);
    this.MANY2(() => this.SUBRULE(this.else_if_clause));
    this.OPTION(() => this.SUBRULE(this.else_clause));
  });

  if_clause = this.RULE('if_clause', () => {
    this.CONSUME(t.If);
    this.SUBRULE(this.expression);
    this.SUBRULE(this.compound_statement);
  });

  else_if_clause = this.RULE('else_if_clause', () => {
    this.CONSUME(t.Else);
    this.CONSUME(t.If);
    this.SUBRULE(this.expression);
    this.SUBRULE(this.compound_statement);
  });

  else_clause = this.RULE('else_clause', () => {
    this.CONSUME(t.Else);
    this.SUBRULE(this.compound_statement);
  });

  switch_statement = this.RULE('switch_statement', () => {
    this.MANY(() => this.SUBRULE(this.attribute));
    this.CONSUME(t.Switch);
    this.SUBRULE(this.expression);
    this.MANY2(() =>
      this.SUBRULE2(this.attribute, { LABEL: 'body_attribute' })
    );
    this.CONSUME(t.LCurly);
    this.MANY3(() => this.SUBRULE(this.switch_clause));
    this.CONSUME(t.RCurly);
  });

  switch_clause = this.RULE('switch_clause', () => {
    this.OR([
      { ALT: () => this.SUBRULE(this.case_clause) },
      { ALT: () => this.SUBRULE(this.default_alone_clause) },
    ]);
  });

  case_clause = this.RULE('case_clause', () => {
    this.CONSUME(t.Case);
    this.SUBRULE(this.case_selector);
    this.MANY(() => {
      this.CONSUME(t.Comma);
      this.SUBRULE2(this.case_selector);
    });
    this.OPTION(() => this.CONSUME2(t.Comma, { LABEL: 'optional_comma' }));
    this.OPTION2(() => this.CONSUME(t.Colon));
    this.SUBRULE(this.compound_statement);
  });

  default_alone_clause = this.RULE('default_alone_clause', () => {
    this.CONSUME(t.Default);
    this.OPTION(() => this.CONSUME(t.Colon));
    this.SUBRULE(this.compound_statement);
  });

  case_selector = this.RULE('case_selector', () => {
    this.OR([
      { ALT: () => this.CONSUME(t.Default) },
      { ALT: () => this.SUBRULE(this.expression) },
    ]);
  });

  loop_statement = this.RULE('loop_statement', () => {
    this.MANY(() => this.SUBRULE(this.attribute));
    this.CONSUME(t.Loop);
    this.MANY2(() =>
      this.SUBRULE2(this.attribute, { LABEL: 'body_attribute' })
    );
    this.CONSUME(t.LCurly);
    this.MANY3(() => this.SUBRULE(this.statement));
    this.OPTION(() => this.SUBRULE(this.continuing_statement));
    this.CONSUME(t.RCurly);
  });

  for_statement = this.RULE('for_statement', () => {
    this.MANY(() => this.SUBRULE(this.attribute));
    this.CONSUME(t.For);
    this.CONSUME(t.LPar);
    this.OPTION(() => this.SUBRULE(this.for_init));
    this.CONSUME(t.Semi);
    this.OPTION2(() =>
      this.SUBRULE(this.expression, { LABEL: 'for_condition' })
    );
    this.CONSUME2(t.Semi);
    this.OPTION3(() => this.SUBRULE(this.for_update));
    this.CONSUME(t.RPar);
    this.SUBRULE(this.compound_statement);
  });

  for_init = this.RULE('for_init', () => {
    this.OR([
      { ALT: () => this.SUBRULE(this.variable_or_value_statement) },
      { ALT: () => this.SUBRULE(this.variable_updating_statement) },
      { ALT: () => this.SUBRULE(this.call_phrase) },
    ]);
  });

  for_update = this.RULE('for_update', () => {
    this.OR([
      { ALT: () => this.SUBRULE(this.variable_updating_statement) },
      { ALT: () => this.SUBRULE(this.call_phrase) },
    ]);
  });

  while_statement = this.RULE('while_statement', () => {
    this.MANY(() => this.SUBRULE(this.attribute));
    this.CONSUME(t.While);
    this.SUBRULE(this.expression);
    this.SUBRULE(this.compound_statement);
  });

  break_statement = this.RULE('break_statement', () => {
    this.CONSUME(t.Break);
    this.CONSUME5(t.Semi);
  });

  break_if_statement = this.RULE('break_if_statement', () => {
    this.CONSUME(t.Break);
    this.CONSUME(t.If);
    this.SUBRULE(this.expression);
    this.CONSUME(t.Semi);
  });

  continue_statement = this.RULE('continue_statement', () => {
    this.CONSUME(t.Continue);
    this.CONSUME(t.Semi);
  });

  discard_statement = this.RULE('discard_statement', () => {
    this.CONSUME(t.Discard);
    this.CONSUME(t.Semi);
  });

  continuing_statement = this.RULE('continuing_statement', () => {
    this.CONSUME(t.Continuing);
    this.MANY(() => this.SUBRULE(this.attribute));
    this.CONSUME(t.LCurly);
    this.MANY2(() => this.SUBRULE(this.statement));
    this.OPTION(() => this.SUBRULE(this.break_if_statement));
    this.CONSUME(t.RCurly);
  });

  return_statement = this.RULE('return_statement', () => {
    this.CONSUME(t.Return);
    this.OPTION(() => this.SUBRULE(this.expression));
    this.CONSUME2(t.Semi);
  });

  const_assert_statement = this.RULE('const_assert_statement', () => {
    this.CONSUME(t.ConstAssert);
    this.SUBRULE(this.expression);
    this.CONSUME5(t.Semi);
  });

  statement = this.RULE('statement', () => {
    this.OR([
      { ALT: () => this.CONSUME(t.Semi) },
      { ALT: () => this.CONSUME(t.Comment) },
      { ALT: () => this.SUBRULE(this.return_statement) },
      { ALT: () => this.SUBRULE(this.break_statement) },
      { ALT: () => this.SUBRULE(this.continue_statement) },
      { ALT: () => this.SUBRULE(this.discard_statement) },
      { ALT: () => this.SUBRULE(this.const_assert_statement) },
      {
        GATE: this.BACKTRACK(this.if_statement),
        ALT: () => this.SUBRULE(this.if_statement),
      },
      {
        GATE: this.BACKTRACK(this.switch_statement),
        ALT: () => this.SUBRULE(this.switch_statement),
      },
      {
        GATE: this.BACKTRACK(this.loop_statement),
        ALT: () => this.SUBRULE(this.loop_statement),
      },
      {
        GATE: this.BACKTRACK(this.for_statement),
        ALT: () => this.SUBRULE(this.for_statement),
      },
      {
        GATE: this.BACKTRACK(this.while_statement),
        ALT: () => this.SUBRULE(this.while_statement),
      },
      {
        ALT: () => {
          this.SUBRULE(this.call_phrase);
          this.CONSUME3(t.Semi);
        },
      },
      {
        ALT: () => {
          this.SUBRULE(this.variable_or_value_statement);
          this.CONSUME4(t.Semi);
        },
      },
      {
        ALT: () => {
          this.SUBRULE(this.variable_updating_statement);
          this.CONSUME8(t.Semi);
        },
      },
      { ALT: () => this.SUBRULE(this.compound_statement) },
    ]);
  });

  variable_updating_statement = this.RULE('variable_updating_statement', () => {
    this.OR([
      { ALT: () => this.SUBRULE(this.phony_assignment) },
      { ALT: () => this.SUBRULE(this.assignment_statement) },
    ]);
  });

  function_declaration = this.RULE('function_declaration', () => {
    this.MANY(() =>
      this.SUBRULE(this.attribute, { LABEL: 'function_attributes' })
    );
    this.CONSUME(t.Fn);
    this.CONSUME(t.IdentPatternToken, { LABEL: 'name' });
    this.CONSUME(t.LPar);
    this.OPTION(() => this.SUBRULE(this.param));
    this.MANY2(() => {
      this.CONSUME(t.Comma);
      this.SUBRULE2(this.param);
    });
    this.OPTION2(() => this.CONSUME2(t.Comma, { LABEL: 'optional_comma' }));
    this.CONSUME(t.RPar);
    this.OPTION3(() => {
      this.CONSUME(t.Arrow);
      this.MANY3(() =>
        this.SUBRULE2(this.attribute, { LABEL: 'return_attributes' })
      );
      this.CONSUME2(t.IdentPatternToken, { LABEL: 'return_type' });
      this.OPTION4(() => this.SUBRULE(this.template_list));
    });
    this.SUBRULE(this.compound_statement);
  });

  param = this.RULE('param', () => {
    this.MANY(() => this.SUBRULE(this.attribute));
    this.CONSUME(t.IdentPatternToken, { LABEL: 'name' });
    this.OPTION2(() => this.SUBRULE(this.type_definition));
  });

  int_literal = this.RULE('int_literal', () => {
    this.OR([
      { ALT: () => this.CONSUME(t.DecimalIntLiteral) },
      { ALT: () => this.CONSUME(t.HexIntLiteral) },
    ]);
  });

  float_literal = this.RULE('float_literal', () => {
    this.OR([
      { ALT: () => this.CONSUME(t.DecimalFloatLiteral) },
      { ALT: () => this.CONSUME(t.HexFloatLiteral) },
    ]);
  });

  literal = this.RULE('literal', () => {
    this.OR([
      { ALT: () => this.SUBRULE(this.int_literal) },
      { ALT: () => this.SUBRULE(this.float_literal) },
      { ALT: () => this.CONSUME(t.BooleanLiteral) },
    ]);
  });
}

export const parser = new WgslParser();
export const BaseWGSLCstVisitor = parser.getBaseCstVisitorConstructor<
  WgslAstParams,
  WgslAstNode
>();
