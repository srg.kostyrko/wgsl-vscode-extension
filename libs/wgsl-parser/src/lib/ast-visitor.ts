import * as a from './ast.types';

const guard = (_i: never) => {
  throw new Error('not exostive switch');
};

export class BaseWgslVisitor<Ctx> {
  visit(node: a.WgslAstNode, ctx: Ctx): void {
    switch (node.ast) {
      case a.WgslAstTypes.AdditiveExpression:
        return this.visitAdditiveExpression(node, ctx);
      case a.WgslAstTypes.AddressOf:
        return this.visitAddressOf(node, ctx);
      case a.WgslAstTypes.AlignAttribute:
        return this.visitAlignAttribute(node, ctx);
      //
      case a.WgslAstTypes.ArrayAccess:
        return this.visitArrayAccess(node, ctx);
      case a.WgslAstTypes.Assignment:
        return this.visitAssignment(node, ctx);
      case a.WgslAstTypes.BinaryExpression:
        return this.visitBinaryExpression(node, ctx);
      case a.WgslAstTypes.BindingAttribute:
        return this.visitBindingAttribute(node, ctx);
      case a.WgslAstTypes.BooleanLiteral:
        return this.visitBooleanLiteral(node, ctx);
      case a.WgslAstTypes.Break:
        return this.visitBreak(node, ctx);
      case a.WgslAstTypes.BreakIf:
        return this.visitBreakIf(node, ctx);
      case a.WgslAstTypes.BuiltinAttribute:
        return this.visitBuiltinAttribute(node, ctx);
      case a.WgslAstTypes.CompoundStatement:
        return this.visitCompoundStatement(node, ctx);
      case a.WgslAstTypes.ComputeAttribute:
        return this.visitComputeAttribute(node, ctx);
      case a.WgslAstTypes.ConstAssert:
        return this.visitConstAssert(node, ctx);
      case a.WgslAstTypes.ConstAttribute:
        return this.visitConstAttribute(node, ctx);
      case a.WgslAstTypes.ConstDeclaration:
        return this.visitConstDeclaration(node, ctx);
      case a.WgslAstTypes.Continue:
        return this.visitContinue(node, ctx);
      case a.WgslAstTypes.Continuing:
        return this.visitContinuing(node, ctx);
      case a.WgslAstTypes.Decrement:
        return this.visitDecrement(node, ctx);
      case a.WgslAstTypes.DiagnosticAttribute:
        return this.visitDiagnosticAttribute(node, ctx);
      case a.WgslAstTypes.DiagnosticDirective:
        return this.visitDiagnosticDirective(node, ctx);
      case a.WgslAstTypes.Discard:
        return this.visitDiscard(node, ctx);
      case a.WgslAstTypes.ElseClause:
        return this.visitElseClause(node, ctx);
      case a.WgslAstTypes.EmptyDeclaration:
        return this.visitEmptyDeclaration(node, ctx);
      case a.WgslAstTypes.EnableDirective:
        return this.visitEnableDirective(node, ctx);
      case a.WgslAstTypes.FloatLiteral:
        return this.visitFloatLiteral(node, ctx);
      case a.WgslAstTypes.For:
        return this.visitFor(node, ctx);
      case a.WgslAstTypes.FragmentAttribute:
        return this.visitFragmentAttribute(node, ctx);
      case a.WgslAstTypes.FunctionCall:
        return this.visitFunctionCall(node, ctx);
      case a.WgslAstTypes.FunctionDeclaration:
        return this.visitFunctionDeclaration(node, ctx);
      case a.WgslAstTypes.FunctionParam:
        return this.visitFunctionParam(node, ctx);
      case a.WgslAstTypes.GlobalConstDeclaration:
        return this.visitGlobalConstDeclaration(node, ctx);
      case a.WgslAstTypes.GlobalVariableDeclaration:
        return this.visitGlobalVariableDeclaration(node, ctx);
      case a.WgslAstTypes.GroupAttribute:
        return this.visitGroupAttribute(node, ctx);
      case a.WgslAstTypes.IdAttribute:
        return this.visitIdAttribute(node, ctx);
      case a.WgslAstTypes.Identifier:
        return this.visitIdentifier(node, ctx);
      case a.WgslAstTypes.If:
        return this.visitIf(node, ctx);
      case a.WgslAstTypes.IfClause:
        return this.visitIfClause(node, ctx);
      case a.WgslAstTypes.Increment:
        return this.visitIncrement(node, ctx);
      case a.WgslAstTypes.Indirection:
        return this.visitIndirection(node, ctx);
      case a.WgslAstTypes.IntegerLiteral:
        return this.visitIntegerLiteral(node, ctx);
      case a.WgslAstTypes.InterpolateAttribute:
        return this.visitInterpolateAttribute(node, ctx);
      case a.WgslAstTypes.InvariantAttribute:
        return this.visitInvariantAttribute(node, ctx);
      case a.WgslAstTypes.LetDeclaration:
        return this.visitLetDeclaration(node, ctx);
      case a.WgslAstTypes.LocationAttribute:
        return this.visitLocationAttribute(node, ctx);
      case a.WgslAstTypes.LogicExpression:
        return this.visitLogicExpression(node, ctx);
      case a.WgslAstTypes.Loop:
        return this.visitLoop(node, ctx);
      case a.WgslAstTypes.MemeberAccess:
        return this.visitMemeberAccess(node, ctx);
      case a.WgslAstTypes.MultiplicativeExpression:
        return this.visitMultiplicativeExpression(node, ctx);
      case a.WgslAstTypes.MustUseAttribute:
        return this.visitMustUseAttribute(node, ctx);
      case a.WgslAstTypes.OverrideDeclaration:
        return this.visitOverrideDeclaration(node, ctx);
      case a.WgslAstTypes.ParentesisExpression:
        return this.visitParentesisExpression(node, ctx);
      case a.WgslAstTypes.PhonyAssignment:
        return this.visitPhonyAssignment(node, ctx);
      case a.WgslAstTypes.Programm:
        return this.visitProgramm(node, ctx);
      case a.WgslAstTypes.RelationalExpression:
        return this.visitRelationalExpression(node, ctx);
      case a.WgslAstTypes.RequiresDirective:
        return this.visitRequiresDirective(node, ctx);
      case a.WgslAstTypes.Return:
        return this.visitReturn(node, ctx);
      case a.WgslAstTypes.ShiftExpression:
        return this.visitShiftExpression(node, ctx);
      case a.WgslAstTypes.SizeAttribute:
        return this.visitSizeAttribute(node, ctx);
      case a.WgslAstTypes.Statement:
        return this.visitStatement(node, ctx);
      case a.WgslAstTypes.StructDeclaration:
        return this.visitStructDeclaration(node, ctx);
      case a.WgslAstTypes.StructMember:
        return this.visitStructMember(node, ctx);
      case a.WgslAstTypes.Switch:
        return this.visitSwitch(node, ctx);
      case a.WgslAstTypes.SwitchCase:
        return this.visitSwitchCase(node, ctx);
      case a.WgslAstTypes.SwitchDefault:
        return this.visitSwitchDefault(node, ctx);
      case a.WgslAstTypes.SwizzeAccess:
        return this.visitSwizzeAccess(node, ctx);
      case a.WgslAstTypes.TemplateList:
        return this.visitTemplateList(node, ctx);
      case a.WgslAstTypes.TypeAlias:
        return this.visitTypeAlias(node, ctx);
      case a.WgslAstTypes.TypeDefinition:
        return this.visitTypeDefinition(node, ctx);
      case a.WgslAstTypes.UnaryExpression:
        return this.visitUnaryExpression(node, ctx);
      case a.WgslAstTypes.Unknown:
        return this.visitUnknown(node, ctx);
      case a.WgslAstTypes.VariableDeclaration:
        return this.visitVariableDeclaration(node, ctx);
      case a.WgslAstTypes.VertexAttribute:
        return this.visitVertexAttribute(node, ctx);
      case a.WgslAstTypes.While:
        return this.visitWhile(node, ctx);
      case a.WgslAstTypes.WorkgroupSizeAttribute:
        return this.visitWorkgroupSizeAttribute(node, ctx);
      case a.WgslAstTypes.Comment:
        return this.visitComment(node, ctx);
      default:
        guard(node);
    }
  }

  visitAdditiveExpression(node: a.WgslAstAdditiveExpression, ctx: Ctx): void {
    this.visit(node.lhs, ctx);
    this.visit(node.rhs, ctx);
  }

  visitAddressOf(node: a.WgslAstAddressOf, ctx: Ctx): void {
    this.visit(node.reference, ctx);
  }

  visitAlignAttribute(node: a.WgslAstAlignAttribute, ctx: Ctx): void {
    this.visit(node.size, ctx);
  }

  visitArrayAccess(node: a.WgslAstArrayAccess, ctx: Ctx): void {
    this.visit(node.index, ctx);
    if (node.specifier) this.visit(node.specifier, ctx);
  }

  visitAssignment(node: a.WgslAstAssignment, ctx: Ctx): void {
    this.visit(node.lhs, ctx);
    this.visit(node.value, ctx);
  }

  visitBinaryExpression(node: a.WgslAstBinaryExpression, ctx: Ctx): void {
    this.visit(node.lhs, ctx);
    this.visit(node.rhs, ctx);
  }

  visitBindingAttribute(node: a.WgslAstBindingAttribute, ctx: Ctx): void {
    this.visit(node.number, ctx);
  }

  visitBooleanLiteral(_node: a.WgslAstBooleanLiteral, _ctx: Ctx): void {
    // noop by default
  }

  visitBreak(_node: a.WgslAstBreak, _ctx: Ctx): void {
    // noop by default
  }

  visitBreakIf(node: a.WgslAstBreakIf, ctx: Ctx): void {
    this.visit(node.condition, ctx);
  }

  visitBuiltinAttribute(node: a.WgslAstBuiltinAttribute, ctx: Ctx): void {
    this.visit(node.associated_object, ctx);
  }

  visitCompoundStatement(node: a.WgslAstCompoundStatement, ctx: Ctx): void {
    for (const attribute of node.attributes) {
      this.visit(attribute, ctx);
    }
    for (const statement of node.statements) {
      this.visit(statement, ctx);
    }
  }

  visitComputeAttribute(_node: a.WgslAstComputeAttribute, _ctx: Ctx): void {
    // noop by default
  }

  visitConstAssert(node: a.WgslAstConstAssert, ctx: Ctx): void {
    this.visit(node.expression, ctx);
  }

  visitConstAttribute(_node: a.WgslAstConstAttribute, _ctx: Ctx): void {
    // noop by default
  }

  visitConstDeclaration(node: a.WgslAstConstDeclaration, ctx: Ctx): void {
    if (node.type) this.visit(node.type, ctx);
    this.visit(node.value, ctx);
  }

  visitContinue(_node: a.WgslAstContinue, _ctx: Ctx): void {
    // noop by default
  }

  visitContinuing(node: a.WgslAstContinuing, ctx: Ctx): void {
    for (const attribute of node.attributes) {
      this.visit(attribute, ctx);
    }
    for (const statement of node.body) {
      this.visit(statement, ctx);
    }
    if (node.break_if) this.visit(node.break_if, ctx);
  }

  visitDecrement(node: a.WgslAstDecrement, ctx: Ctx): void {
    this.visit(node.lhs, ctx);
  }

  visitDiagnosticAttribute(
    _node: a.WgslAstDiagnosticAttribute,
    _ctx: Ctx
  ): void {
    // noop by default
  }

  visitDiagnosticDirective(
    _node: a.WgslAstDiagnosticDirective,
    _ctx: Ctx
  ): void {
    // noop by default
  }

  visitDiscard(_node: a.WgslAstDiscard, _ctx: Ctx): void {
    // noop by default
  }

  visitElseClause(node: a.WgslAstElseClause, ctx: Ctx): void {
    this.visit(node.body, ctx);
  }

  visitEmptyDeclaration(_node: a.WgslAstEmptyDeclaration, _ctx: Ctx): void {
    // noop by default
  }

  visitEnableDirective(_node: a.WgslAstEnableDirective, _ctx: Ctx): void {
    // noop by default
  }

  visitFloatLiteral(_node: a.WgslAstFloatLiteral, _ctx: Ctx): void {
    // nnop byb default
  }

  visitFor(node: a.WgslAstFor, ctx: Ctx): void {
    if (node.initializer) this.visit(node.initializer, ctx);
    if (node.condition) this.visit(node.condition, ctx);
    if (node.update_part) this.visit(node.update_part, ctx);
    this.visit(node.body, ctx);
  }

  visitFragmentAttribute(_node: a.WgslAstFragmentAttribute, _ctx: Ctx): void {
    // noop by default
  }

  visitFunctionCall(node: a.WgslAstFunctionCall, ctx: Ctx): void {
    if (node.template_list) this.visit(node.template_list, ctx);
    for (const param of node.params) {
      this.visit(param, ctx);
    }
  }

  visitFunctionDeclaration(node: a.WgslAstFunctionDeclaration, ctx: Ctx): void {
    for (const attribute of node.function_attributes) {
      this.visit(attribute, ctx);
    }
    for (const param of node.params) {
      this.visit(param, ctx);
    }
    for (const attribute of node.return_attributes ?? []) {
      this.visit(attribute, ctx);
    }
    if (node.return_template_list) this.visit(node.return_template_list, ctx);
    this.visit(node.body, ctx);
  }

  visitFunctionParam(node: a.WgslAstFunctionParam, ctx: Ctx): void {
    for (const attribute of node.attributes) {
      this.visit(attribute, ctx);
    }
    this.visit(node.type, ctx);
  }

  visitGlobalConstDeclaration(
    node: a.WgslAstGlobalConstDeclaration,
    ctx: Ctx
  ): void {
    this.visit(node.declaration, ctx);
  }

  visitGlobalVariableDeclaration(
    node: a.WgslAstGlobalVariableDeclaration,
    ctx: Ctx
  ): void {
    for (const attribute of node.attributes) {
      this.visit(attribute, ctx);
    }
    this.visit(node.declaration, ctx);
  }

  visitGroupAttribute(node: a.WgslAstGroupAttribute, ctx: Ctx): void {
    this.visit(node.group, ctx);
  }

  visitIdAttribute(node: a.WgslAstIdAttribute, ctx: Ctx): void {
    this.visit(node.identifier, ctx);
  }

  visitIdentifier(node: a.WgslAstIdentifier, ctx: Ctx): void {
    if (node.template_list) this.visit(node.template_list, ctx);
    if (node.specifier) this.visit(node.specifier, ctx);
  }

  visitIf(node: a.WgslAstIf, ctx: Ctx): void {
    for (const attribute of node.attributes) {
      this.visit(attribute, ctx);
    }
    this.visit(node.clause, ctx);
    for (const elseIfClause of node.else_if_clauses) {
      this.visit(elseIfClause, ctx);
    }
    if (node.else_clause) this.visit(node.else_clause, ctx);
  }

  visitIfClause(node: a.WgslAstIfClause, ctx: Ctx): void {
    this.visit(node.condition, ctx);
    this.visit(node.body, ctx);
  }

  visitIncrement(node: a.WgslAstIncrement, ctx: Ctx): void {
    this.visit(node.lhs, ctx);
  }

  visitIndirection(node: a.WgslAstIndirection, ctx: Ctx): void {
    this.visit(node.pointer, ctx);
  }

  visitIntegerLiteral(_node: a.WgslAstIntegerLiteral, _ctx: Ctx): void {
    // noop by default
  }

  visitInterpolateAttribute(
    node: a.WgslAstInterpolateAttribute,
    ctx: Ctx
  ): void {
    this.visit(node.type, ctx);
    if (node.sampling) this.visit(node.sampling, ctx);
  }

  visitInvariantAttribute(_node: a.WgslAstInvariantAttribute, _ctx: Ctx): void {
    // noop be default
  }

  visitLetDeclaration(node: a.WgslAstLetDeclaration, ctx: Ctx): void {
    if (node.type) this.visit(node.type, ctx);
    this.visit(node.value, ctx);
  }

  visitLocationAttribute(node: a.WgslAstLocationAttribute, ctx: Ctx): void {
    this.visit(node.part, ctx);
  }

  visitLogicExpression(node: a.WgslAstLogicExpression, ctx: Ctx): void {
    this.visit(node.lhs, ctx);
    this.visit(node.rhs, ctx);
  }

  visitLoop(node: a.WgslAstLoop, ctx: Ctx): void {
    for (const attribute of node.attributes) {
      this.visit(attribute, ctx);
    }
    for (const attribute of node.body_attributes) {
      this.visit(attribute, ctx);
    }
    for (const statement of node.body) {
      this.visit(statement, ctx);
    }
    if (node.continuing) this.visit(node.continuing, ctx);
  }

  visitMemeberAccess(node: a.WgslAstMemeberAccess, ctx: Ctx): void {
    if (node.specifier) this.visit(node.specifier, ctx);
  }

  visitMultiplicativeExpression(
    node: a.WgslAstMultiplicativeExpression,
    ctx: Ctx
  ): void {
    this.visit(node.lhs, ctx);
    this.visit(node.rhs, ctx);
  }

  visitMustUseAttribute(_node: a.WgslAstMustUseAttribute, _ctx: Ctx): void {
    // noop by default
  }

  visitOverrideDeclaration(node: a.WgslAstOverrideDeclaration, ctx: Ctx): void {
    for (const attribute of node.attributes) {
      this.visit(attribute, ctx);
    }
    if (node.type) this.visit(node.type, ctx);
    if (node.value) this.visit(node.value, ctx);
  }

  visitParentesisExpression(
    node: a.WgslAstParentesisExpression,
    ctx: Ctx
  ): void {
    this.visit(node.expression, ctx);
    if (node.specifier) this.visit(node.specifier, ctx);
  }

  visitPhonyAssignment(node: a.WgslAstPhonyAssignment, ctx: Ctx): void {
    this.visit(node.value, ctx);
  }

  visitProgramm(node: a.WgslAstProgramm, ctx: Ctx): void {
    for (const directive of node.directives) {
      this.visit(directive, ctx);
    }
    for (const declaration of node.declarations) {
      this.visit(declaration, ctx);
    }
  }

  visitRelationalExpression(
    node: a.WgslAstRelationalExpression,
    ctx: Ctx
  ): void {
    this.visit(node.lhs, ctx);
    this.visit(node.rhs, ctx);
  }

  visitRequiresDirective(_node: a.WgslAstRequiresDirective, _ctx: Ctx): void {
    // noop by default
  }

  visitReturn(node: a.WgslAstReturn, ctx: Ctx): void {
    if (node.data) this.visit(node.data, ctx);
  }

  visitShiftExpression(node: a.WgslAstShiftExpression, ctx: Ctx): void {
    this.visit(node.lhs, ctx);
    this.visit(node.rhs, ctx);
  }

  visitSizeAttribute(node: a.WgslAstSizeAttribute, ctx: Ctx): void {
    this.visit(node.size, ctx);
  }

  visitStatement(node: a.WgslAstStatement, ctx: Ctx): void {
    this.visit(node.statement, ctx);
  }

  visitStructDeclaration(node: a.WgslAstStructDeclaration, ctx: Ctx): void {
    for (const member of node.members) {
      this.visit(member, ctx);
    }
  }

  visitStructMember(node: a.WgslAstStructMember, ctx: Ctx): void {
    for (const attribute of node.attributes) {
      this.visit(attribute, ctx);
    }
    this.visit(node.type, ctx);
  }

  visitSwitch(node: a.WgslAstSwitch, ctx: Ctx): void {
    for (const attribute of node.attributes) {
      this.visit(attribute, ctx);
    }
    this.visit(node.condition, ctx);
    for (const attribute of node.body_attributes) {
      this.visit(attribute, ctx);
    }
    for (const switchCase of node.cases) {
      this.visit(switchCase, ctx);
    }
  }

  visitSwitchCase(node: a.WgslAstSwitchCase, ctx: Ctx): void {
    for (const selector of node.selectors) {
      this.visit(selector, ctx);
    }
    this.visit(node.body, ctx);
  }

  visitSwitchDefault(node: a.WgslAstSwitchDefault, ctx: Ctx): void {
    if (node.body) this.visit(node.body, ctx);
  }

  visitSwizzeAccess(node: a.WgslAstSwizzeAccess, ctx: Ctx): void {
    if (node.specifier) this.visit(node.specifier, ctx);
  }

  visitTemplateList(node: a.WgslAstTemplateList, ctx: Ctx): void {
    for (const member of node.members) {
      this.visit(member, ctx);
    }
  }

  visitTypeAlias(node: a.WgslAstTypeAlias, ctx: Ctx): void {
    if (node.template_list) this.visit(node.template_list, ctx);
  }

  visitTypeDefinition(node: a.WgslAstTypeDefinition, ctx: Ctx): void {
    if (node.template_list) this.visit(node.template_list, ctx);
  }

  visitUnaryExpression(node: a.WgslAstUnaryExpression, ctx: Ctx): void {
    this.visit(node.rhs, ctx);
  }

  visitUnknown(_node: a.WgslAstUnknown, _ctx: Ctx): void {
    // noop by default
  }

  visitVariableDeclaration(node: a.WgslAstVariableDeclaration, ctx: Ctx): void {
    if (node.template_list) this.visit(node.template_list, ctx);
    if (node.type) this.visit(node.type, ctx);
    if (node.value) this.visit(node.value, ctx);
  }

  visitVertexAttribute(_node: a.WgslAstVertexAttribute, _ctx: Ctx): void {
    // noop by default
  }

  visitWhile(node: a.WgslAstWhile, ctx: Ctx): void {
    for (const attribute of node.attributes) {
      this.visit(attribute, ctx);
    }
    this.visit(node.condition, ctx);
    this.visit(node.body, ctx);
  }

  visitWorkgroupSizeAttribute(
    node: a.WgslAstWorkgroupSizeAttribute,
    ctx: Ctx
  ): void {
    this.visit(node.x, ctx);
    if (node.y) this.visit(node.y, ctx);
    if (node.z) this.visit(node.z, ctx);
  }

  visitComment(_node: a.WgslAstComment, _ctx: Ctx): void {
    // noop by default
  }
}
