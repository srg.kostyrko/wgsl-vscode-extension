import { IToken, Lexer } from 'chevrotain';
import {
  AmpAmp,
  Colon,
  Eq,
  Gt,
  IdentPatternToken,
  LBrack,
  LCurly,
  LPar,
  Lt,
  PipePipe,
  RBrack,
  RPar,
  Semi,
  TemplateArgsEnd,
  TemplateArgsStart,
  Var,
  allTokens,
} from './tokens';

export const WgslLexer = new Lexer(allTokens, {
  skipValidations: false,
  positionTracking: 'onlyOffset',
});

type UnclosedCandidate = {
  position: number;
  depth: number;
};
type TemplateList = {
  start: number;
  end: number;
};

// source https://gpuweb.github.io/gpuweb/wgsl/#template-list-discovery
export function disambiguateTemplateLists(tokens: IToken[]): IToken[] {
  const discoveredTemplateLists: TemplateList[] = [];
  const pending: UnclosedCandidate[] = [];
  let currentPosition = 0;
  let currentDepth = 0;

  while (currentPosition < tokens.length) {
    const { tokenType } = tokens[currentPosition];
    if (tokenType === Var || tokenType === IdentPatternToken) {
      const nextToken = tokens[currentPosition + 1];
      if (nextToken?.tokenType === Lt) {
        currentPosition++;
        pending.push({
          position: currentPosition,
          depth: currentDepth,
        });
      }
    } else if (tokenType === Gt) {
      const topPending = pending.at(-1);
      if (topPending && topPending.depth === currentDepth) {
        discoveredTemplateLists.push({
          start: topPending.position,
          end: currentPosition,
        });
        pending.pop();
      }
    } else if (tokenType === LPar || tokenType === LBrack) {
      // ?check for pending
      currentDepth++;
    } else if (tokenType === RPar || tokenType === RBrack) {
      while ((pending.at(-1)?.depth ?? -1) >= currentDepth) {
        pending.pop();
      }
      currentDepth = Math.max(0, currentDepth - 1);
    } else if (tokenType === Eq) {
      currentDepth = 0;
      pending.length = 0;
    } else if (
      tokenType === Semi ||
      tokenType === LCurly ||
      tokenType === Colon
    ) {
      currentDepth = 0;
      pending.length = 0;
    } else if (tokenType === AmpAmp || tokenType === PipePipe) {
      while ((pending.at(-1)?.depth ?? -1) >= currentDepth) {
        pending.pop();
      }
    }
    currentPosition++;
  }

  if (discoveredTemplateLists.length > 0) {
    const updatedTokens = tokens.slice();
    for (const { start, end } of discoveredTemplateLists) {
      const startToken = tokens[start];
      const endToken = tokens[end];
      updatedTokens[start] = {
        ...startToken,
        tokenType: TemplateArgsStart,
        tokenTypeIdx: TemplateArgsStart.tokenTypeIdx ?? startToken.tokenTypeIdx,
      };
      updatedTokens[end] = {
        ...endToken,
        tokenType: TemplateArgsEnd,
        tokenTypeIdx: TemplateArgsEnd.tokenTypeIdx ?? endToken.tokenTypeIdx,
      };
    }
    return updatedTokens;
  }

  return tokens;
}
