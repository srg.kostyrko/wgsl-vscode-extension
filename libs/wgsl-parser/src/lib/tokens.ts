import { IToken, Lexer, createToken } from 'chevrotain';

export const BlankSpace = createToken({
  name: 'BlankSpace',
  pattern: /\s+/,
  line_breaks: true,
  group: Lexer.SKIPPED,
});

export const Comment = createToken({
  name: 'Comment',
  pattern: Lexer.NA,
})

const SingleLineComment = createToken({
  name: 'SingleLineComment',
  pattern: /\/\/.*/,
  categories: [Comment],
  line_breaks: false,
});
const MultiLineComment = createToken({
  name: 'MultiLineComment',
  pattern: /\/\*[^*]*\*+([^/*][^*]*\*+)*\//,
  categories: [Comment],
  line_breaks: true
});

const ReservedKeygord = createToken({
  name: 'ReservedKeygord',
  pattern:
    /\b(NULL|Self|abstract|active|alignas|alignof|as|asm|asm_fragment|async|attribute|auto|await|become|binding_array|cast|catch|class|co_await|co_return|co_yield|coherent|column_major|common|compile|compile_fragment|concept|const_cast|consteval|constexpr|constinit|crate|debugger|decltype|delete|demote|demote_to_helper|do|dynamic_cast|enum|explicit|export|extends|extern|external|fallthrough|filter|final|finally|friend|from|fxgroup|get|goto|groupshared|highp|impl|implements|import|inline|instanceof|interface|layout|lowp|macro|macro_rules|match|mediump|meta|mod|module|move|mut|mutable|namespace|new|nil|noexcept|noinline|nointerpolation|noperspective|null|nullptr|of|operator|package|packoffset|partition|pass|patch|pixelfragment|precise|precision|premerge|priv|protected|pub|public|readonly|ref|regardless|register|reinterpret_cast|require|resource|restrict|self|set|shared|sizeof|smooth|snorm|static|static_assert|static_cast|std|subroutine|super|target|template|this|thread_local|throw|trait|try|type|typedef|typeid|typename|typeof|union|unless|unorm|unsafe|unsized|use|using|varying|virtual|volatile|wgsl|where|with|writeonly|yield)\b/,
});

export const AssignmentOperator = createToken({
  name: 'CompaundAssignmentOperator',
  pattern: Lexer.NA,
});

export const LogicOperator = createToken({
  name: 'LogicOperator',
  pattern: Lexer.NA,
});

export const UnaryOperator = createToken({
  name: 'UnaryOperator',
  pattern: Lexer.NA,
});

export const BinaryOperator = createToken({
  name: 'BinaryOperator',
  pattern: Lexer.NA,
});

export const RelationalOperator = createToken({
  name: 'RelationalOperator',
  pattern: Lexer.NA,
});

export const ShiftOperator = createToken({
  name: 'ShiftOperator',
  pattern: Lexer.NA,
});

export const MultiplicativeOperator = createToken({
  name: 'MultiplicativeOperator',
  pattern: Lexer.NA,
});

export const AdditiveOperator = createToken({
  name: 'AdditiveOperator',
  pattern: Lexer.NA,
});

export const AttributeSingleParam = createToken({
  name: 'AttributeSingleParam',
  pattern: Lexer.NA,
});
export const AttributeNoParam = createToken({
  name: 'AttributeNoParam',
  pattern: Lexer.NA,
});

export const TemplateArgsStart = createToken({
  name: 'TemplateArgsStart',
  pattern: Lexer.NA,
});
export const TemplateArgsEnd = createToken({
  name: 'TemplateArgsEnd',
  pattern: Lexer.NA,
});

const identTokenPattern =
  /([_\p{XID_Start}][\p{XID_Continue}]+)|([\p{XID_Start}])/uy;
export const IdentPatternToken = createToken({
  name: 'IdentPatternToken',
  pattern: function (
    text: string,
    startOffset: number
  ): RegExpExecArray | null {
    identTokenPattern.lastIndex = startOffset;
    const execResult = identTokenPattern.exec(text);
    return execResult;
  },
});

export const Diagnostic = createToken({
  name: 'Diagnostic',
  pattern: /\bdiagnostic\b/,
});

export const Struct = createToken({
  name: 'Struct',
  pattern: /\bstruct\b/,
});

export const Enable = createToken({
  name: 'Enable',
  pattern: /\benable\b/,
});

export const Requires = createToken({
  name: 'Requires',
  pattern: /\brequires\b/,
});

export const SeverityControlName = createToken({
  name: 'SeverityControlName',
  pattern: /\b(error|info|off|warning)\b/,
});

export const Fn = createToken({
  name: 'Fn',
  pattern: /\bfn\b/,
});

export const Var = createToken({
  name: 'Var',
  pattern: /\bvar\b/,
});
export const Const = createToken({
  name: 'Const',
  pattern: /\bconst\b/,
});
export const Let = createToken({
  name: 'Let',
  pattern: /\blet\b/,
});

export const For = createToken({
  name: 'For',
  pattern: /\bfor\b/,
});

export const If = createToken({
  name: 'If',
  pattern: /\bif\b/,
});

export const Else = createToken({
  name: 'Else',
  pattern: /\belse\b/,
});

export const Loop = createToken({
  name: 'Loop',
  pattern: /\bloop\b/,
});

export const Continuing = createToken({
  name: 'Continuing',
  pattern: /\bcontinuing\b/,
});

export const Break = createToken({
  name: 'break',
  pattern: /\bbreak\b/,
});

export const Switch = createToken({
  name: 'Switch',
  pattern: /\bswitch\b/,
});

export const While = createToken({
  name: 'While',
  pattern: /\bwhile\b/,
});

export const Override = createToken({
  name: 'Override',
  pattern: /\boverride\b/,
});

export const Discard = createToken({
  name: 'Discard',
  pattern: /\bdiscard\b/,
});

export const Return = createToken({
  name: 'Return',
  pattern: /\breturn\b/,
});

export const Continue = createToken({
  name: 'Continue',
  pattern: /\bcontinue\b/,
});

export const Default = createToken({
  name: 'Default',
  pattern: /\bdefault\b/,
});

export const Case = createToken({
  name: 'Case',
  pattern: /\bcase\b/,
});

export const Alias = createToken({
  name: 'Alias',
  pattern: /\balias\b/,
});

export const ConstAssert = createToken({
  name: 'ConstAssert',
  pattern: /\bconst_assert\b/,
});

export const AllignAttr = createToken({
  name: 'AllignAttr',
  pattern: /@align\b/,
  categories: [AttributeSingleParam],
});

export const BindingAttr = createToken({
  name: 'BindingAttr',
  pattern: /@binding\b/,
  categories: [AttributeSingleParam],
});

export const BuiltinAttr = createToken({
  name: 'BuiltinAttr',
  pattern: /@builtin\b/,
  categories: [AttributeSingleParam],
});

export const ComputeAttr = createToken({
  name: 'ComputeAttr',
  pattern: /@compute\b/,
  categories: [AttributeNoParam],
});

export const ConstAttr = createToken({
  name: 'ConstAttr',
  pattern: /@const\b/,
  categories: [AttributeNoParam],
});

export const DiagnosticAttr = createToken({
  name: 'DiagnosticAttr',
  pattern: /@diagnostic\b/,
});

export const FragmentAttr = createToken({
  name: 'FragmentAttr',
  pattern: /@fragment\b/,
  categories: [AttributeNoParam],
});

export const GroupAttr = createToken({
  name: 'GroupAttr',
  pattern: /@group\b/,
  categories: [AttributeSingleParam],
});

export const IdAttr = createToken({
  name: 'IdAttr',
  pattern: /@id\b/,
  categories: [AttributeSingleParam],
});

export const InterpolateAttr = createToken({
  name: 'InterpolateAttr',
  pattern: /@interpolate\b/,
});

export const InvariantAttr = createToken({
  name: 'InvariantAttr',
  pattern: /@invariant\b/,
  categories: [AttributeNoParam],
});

export const LocationAttr = createToken({
  name: 'LocationAttr',
  pattern: /@location\b/,
  categories: [AttributeSingleParam],
});

export const MustUseAttr = createToken({
  name: 'MustUseAttr',
  pattern: /@must_use\b/,
  categories: [AttributeNoParam],
});

export const SizeAttr = createToken({
  name: 'SizeAttr',
  pattern: /@size\b/,
  categories: [AttributeSingleParam],
});

export const VertexAttr = createToken({
  name: 'VertexAttr',
  pattern: /@vertex\b/,
  categories: [AttributeNoParam],
});

export const WorkgroupSizeAttr = createToken({
  name: 'WorkgroupSizeAttr',
  pattern: /@workgroup_size\b/,
});

const swizzlePattern = /[rgba]{1,4}|[xyzw]{1,4}/y;
export const SwizzleName = createToken({
  name: 'SwizzleName',
  pattern: function (
    text: string,
    startOffset: number,
    tokens: IToken[]
  ): RegExpExecArray | null {
    const lastToken = tokens.at(-1);
    if (lastToken?.tokenType === Dot) {
      swizzlePattern.lastIndex = startOffset;
      const execResult = swizzlePattern.exec(text);
      return execResult;
    }
    return null;
  },
  longer_alt: IdentPatternToken,
});

export const BooleanLiteral = createToken({
  name: 'BooleanLiteral',
  pattern: /false|true/,
});

export const LPar = createToken({
  name: 'LPar',
  pattern: /\(/,
});

export const RPar = createToken({
  name: 'RPar',
  pattern: /\)/,
});
export const LCurly = createToken({
  name: 'LCurly',
  pattern: /\{/,
});

export const RCurly = createToken({
  name: 'RCurly',
  pattern: /\}/,
});

export const LBrack = createToken({
  name: 'LBrack',
  pattern: /\[/,
});

export const RBrack = createToken({
  name: 'RBrack',
  pattern: /\]/,
});

export const Arrow = createToken({
  name: 'Arrow',
  pattern: /->/,
});

export const Comma = createToken({
  name: 'Comma',
  pattern: /,/,
});

export const Dot = createToken({
  name: 'Dot',
  pattern: /\./,
});

export const Semi = createToken({
  name: 'Semi',
  pattern: /;/,
});

export const Colon = createToken({
  name: 'Colon',
  pattern: /:/,
});

export const EqEq = createToken({
  name: 'EqEq',
  pattern: /==/,
});

export const Eq = createToken({
  name: 'Eq',
  pattern: /=/,
  categories: [AssignmentOperator],
});

export const Neq = createToken({
  name: 'Neq',
  pattern: /!=/,
  categories: [RelationalOperator],
});

export const ShiftLeft = createToken({
  name: 'ShiftLeft',
  pattern: /<</,
  categories: [ShiftOperator],
});
export const ShiftRight = createToken({
  name: 'ShiftRight',
  pattern: />>/,
  categories: [ShiftOperator],
});

export const Lte = createToken({
  name: 'Lte',
  pattern: /<=/,
  categories: [RelationalOperator],
});

export const Lt = createToken({
  name: 'Lt',
  pattern: /</,
  categories: [RelationalOperator],
});

export const Gte = createToken({
  name: 'Gte',
  pattern: />=/,
  categories: [RelationalOperator],
});

export const Gt = createToken({
  name: 'Gt',
  pattern: />/,
  categories: [RelationalOperator],
});

export const PlusPlus = createToken({
  name: 'PlusPlus',
  pattern: /\+\+/,
});
export const MinusMinus = createToken({
  name: 'MinusMinus',
  pattern: /--/,
});
export const Underscore = createToken({
  name: 'Unserscore',
  pattern: /_/,
});

export const ShiftLeftAssign = createToken({
  name: 'ShiftLeftAssign',
  pattern: />>=/,
  categories: [AssignmentOperator],
});
export const ShiftRightAssign = createToken({
  name: 'ShiftRightAssign',
  pattern: /<<=/,
  categories: [AssignmentOperator],
});

export const ModAssign = createToken({
  name: 'ModAssign',
  pattern: /%=/,
  categories: [AssignmentOperator],
});

export const AndAssign = createToken({
  name: 'AndAssign',
  pattern: /&=/,
  categories: [AssignmentOperator],
});

export const MultAssign = createToken({
  name: 'MultAssign',
  pattern: /\*=/,
  categories: [AssignmentOperator],
});

export const PlusAssign = createToken({
  name: 'PlusAssign',
  pattern: /\+=/,
  categories: [AssignmentOperator],
});

export const MinusAssign = createToken({
  name: 'MinusAssign',
  pattern: /-=/,
  categories: [AssignmentOperator],
});

export const DivAssign = createToken({
  name: 'DivAssign',
  pattern: /\/=/,
  categories: [AssignmentOperator],
});

export const Xor = createToken({
  name: 'Xor',
  pattern: /\^/,
  categories: [BinaryOperator],
});

export const XorAssign = createToken({
  name: 'XorAssign',
  pattern: /\^=/,
  categories: [AssignmentOperator],
});

export const OrAssign = createToken({
  name: 'OrAssign',
  pattern: /\|=/,
  categories: [AssignmentOperator],
});

export const Pipe = createToken({
  name: 'Pipe',
  pattern: /\|/,
  categories: [BinaryOperator],
});

export const PipePipe = createToken({
  name: 'PipePipe',
  pattern: /\|\|/,
  categories: [LogicOperator],
});

export const AmpAmp = createToken({
  name: 'AmpAmp',
  pattern: /&&/,
  categories: [LogicOperator],
});

export const Amp = createToken({
  name: 'Amp',
  pattern: /&/,
  categories: [UnaryOperator, BinaryOperator],
});

export const Minus = createToken({
  name: 'Minus',
  pattern: /-/,
  categories: [UnaryOperator, AdditiveOperator],
});

export const Mult = createToken({
  name: 'Mult',
  pattern: /\*/,
  categories: [UnaryOperator, MultiplicativeOperator],
});

export const Esclamation = createToken({
  name: 'Esclamation',
  pattern: /!/,
  categories: [UnaryOperator],
});

export const Tilda = createToken({
  name: 'Tilda',
  pattern: /~/,
  categories: [UnaryOperator],
});

export const Div = createToken({
  name: 'Div',
  pattern: /\//,
  categories: [MultiplicativeOperator],
});

export const Mod = createToken({
  name: 'Mod',
  pattern: /%/,
  categories: [MultiplicativeOperator],
});

export const Plus = createToken({
  name: 'Mod',
  pattern: /\+/,
  categories: [AdditiveOperator],
});

const decimalFloatPatterns = [
  /[0-9]*\.[0-9]+([eE][+-]?[0-9]+)?[fh]?/y,
  /[0-9]+[eE][+-]?[0-9]+[fh]?/y,
  /[0-9]+\.[0-9]*([eE][+-]?[0-9]+)?[fh]?/y,
  /0|([1-9][0-9]*)[fh]/y,
];
const hexFloatPatterns = [
  /0[xX][0-9a-fA-F]*\.[0-9a-fA-F]+([pP][+-]?[0-9]+[fh]?)?/y,
  /0[xX][0-9a-fA-F]+[pP][+-]?[0-9]+[fh]?/y,
  /0[xX][0-9a-fA-F]+\.[0-9a-fA-F]*([pP][+-]?[0-9]+[fh]?)?/y,
];

function buildArrayMatcher(patterns: RegExp[]) {
  return function (text: string, startOffset: number): RegExpExecArray | null {
    for (const pattern of patterns) {
      pattern.lastIndex = startOffset;
      const execResult = pattern.exec(text);
      if (execResult !== null) return execResult;
    }
    return null;
  };
}

export const DecimalFloatLiteral = createToken({
  name: 'DecimalFloatLiteral',
  pattern: buildArrayMatcher(decimalFloatPatterns),
  line_breaks: false,
});

export const HexFloatLiteral = createToken({
  name: 'HexFloatLiteral',
  pattern: buildArrayMatcher(hexFloatPatterns),
  line_breaks: false,
});

export const DecimalIntLiteral = createToken({
  name: 'DecimalIntLiteral',
  pattern: /(0|[1-9][0-9]*)[iu]?/y,
});

export const HexIntLiteral = createToken({
  name: 'HexIntLiteral',
  pattern: /0[xX][0-9a-fA-F]+[iu]?/,
});

export const allTokens = [
  BlankSpace,
  SingleLineComment,
  MultiLineComment,
  TemplateArgsStart,
  TemplateArgsEnd,
  Struct,
  Diagnostic,
  Enable,
  Requires,
  SeverityControlName,
  Fn,
  Var,
  Const,
  Let,
  Alias,
  Override,
  Discard,
  Return,
  ConstAssert,
  AllignAttr,
  BindingAttr,
  BuiltinAttr,
  ComputeAttr,
  ConstAttr,
  DiagnosticAttr,
  FragmentAttr,
  GroupAttr,
  IdAttr,
  InterpolateAttr,
  InvariantAttr,
  LocationAttr,
  MustUseAttr,
  SizeAttr,
  VertexAttr,
  WorkgroupSizeAttr,
  For,
  If,
  Else,
  Loop,
  Continuing,
  Break,
  Switch,
  While,
  Continue,
  Default,
  Case,
  ReservedKeygord,
  SwizzleName,
  // identifier should be after keywords
  IdentPatternToken,
  Semi,
  LPar,
  RPar,
  LCurly,
  RCurly,
  LBrack,
  RBrack,
  Comma,
  Dot,
  Arrow,
  Colon,
  PlusPlus,
  MinusMinus,
  Underscore,
  AssignmentOperator,
  ShiftLeftAssign,
  ShiftRightAssign,
  ModAssign,
  AndAssign,
  MultAssign,
  PlusAssign,
  MinusAssign,
  DivAssign,
  XorAssign,
  OrAssign,
  PipePipe,
  UnaryOperator,
  RelationalOperator,
  AdditiveOperator,
  MultiplicativeOperator,
  Neq,
  Esclamation,
  ShiftLeft,
  ShiftRight,
  Lte,
  Lt,
  Gte,
  Gt,
  EqEq,
  Eq,
  AmpAmp,
  Amp,
  Mult,
  Minus,
  Tilda,
  Xor,
  Pipe,
  Div,
  Mod,
  Plus,
  DecimalFloatLiteral,
  HexFloatLiteral,
  DecimalIntLiteral,
  HexIntLiteral,
];
