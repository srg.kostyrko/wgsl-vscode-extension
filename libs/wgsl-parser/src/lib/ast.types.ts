export type WgslAstParams = {
  //
};

export enum WgslAstTypes {
  Programm,
  DiagnosticDirective,
  EnableDirective,
  RequiresDirective,
  EmptyDeclaration,
  TemplateList,
  TypeDefinition,
  FunctionDeclaration,
  FunctionParam,
  Statement,
  CompoundStatement,

  // Attributes
  DiagnosticAttribute,
  InterpolateAttribute,
  WorkgroupSizeAttribute,
  ConstAttribute,
  InvariantAttribute,
  MustUseAttribute,
  VertexAttribute,
  FragmentAttribute,
  ComputeAttribute,
  AlignAttribute,
  BindingAttribute,
  BuiltinAttribute,
  GroupAttribute,
  IdAttribute,
  LocationAttribute,
  SizeAttribute,

  StructDeclaration,
  StructMember,
  TypeAlias,

  While,
  For,
  Loop,
  Switch,
  SwitchCase,
  SwitchDefault,

  Break,
  BreakIf,
  Continue,
  Continuing,
  Return,
  ConstAssert,
  Discard,
  If,
  IfClause,
  ElseClause,

  Assignment,
  PhonyAssignment,
  Increment,
  Decrement,
  FunctionCall,

  ParentesisExpression,
  UnaryExpression,
  AdditiveExpression,
  MultiplicativeExpression,
  BinaryExpression,
  ShiftExpression,
  RelationalExpression,
  LogicExpression,

  AddressOf,
  Indirection,
  Identifier,
  ArrayAccess,
  MemeberAccess,
  SwizzeAccess,

  GlobalConstDeclaration,
  GlobalVariableDeclaration,
  OverrideDeclaration,
  VariableDeclaration,
  ConstDeclaration,
  LetDeclaration,

  BooleanLiteral,
  IntegerLiteral,
  FloatLiteral,
  Comment,
  Unknown,
}

export interface AstOffsets {
  start: number;
  end: number;
}

export interface AstToken {
  text: string;
  start: number;
  end: number;
}

export type WgslAstNode =
  | WgslAstProgramm
  | WgslAstFunctionDeclaration
  | WgslAstTypeDefinition
  | WgslAstFunctionParam
  | WgslAstDiagnosticDirective
  | WgslAstEnableDirective
  | WgslAstRequiresDirective
  | WgslAstStructDeclaration
  | WgslAstStructMember
  | WgslAstTypeAlias
  | WgslAstStatement
  | WgslAstCompoundStatement
  | WgslAstEmptyDeclaration
  | WgslAstTemplateList
  | WgslAstDiagnosticAttribute
  | WgslAstInterpolateAttribute
  | WgslAstWorkgroupSizeAttribute
  | WgslAstConstAttribute
  | WgslAstInvariantAttribute
  | WgslAstMustUseAttribute
  | WgslAstVertexAttribute
  | WgslAstFragmentAttribute
  | WgslAstComputeAttribute
  | WgslAstAlignAttribute
  | WgslAstBindingAttribute
  | WgslAstBuiltinAttribute
  | WgslAstGroupAttribute
  | WgslAstIdAttribute
  | WgslAstLocationAttribute
  | WgslAstSizeAttribute
  | WgslAstBreak
  | WgslAstBreakIf
  | WgslAstContinue
  | WgslAstContinuing
  | WgslAstReturn
  | WgslAstConstAssert
  | WgslAstDiscard
  | WgslAstWhile
  | WgslAstFor
  | WgslAstLoop
  | WgslAstSwitch
  | WgslAstSwitchCase
  | WgslAstSwitchDefault
  | WgslAstIf
  | WgslAstIfClause
  | WgslAstElseClause
  | WgslAstIncrement
  | WgslAstDecrement
  | WgslAstAssignment
  | WgslAstPhonyAssignment
  | WgslAstBooleanLiteral
  | WgslAstIntegerLiteral
  | WgslAstFloatLiteral
  | WgslAstUnaryExpression
  | WgslAstParentesisExpression
  | WgslAstAdditiveExpression
  | WgslAstMultiplicativeExpression
  | WgslAstBinaryExpression
  | WgslAstShiftExpression
  | WgslAstRelationalExpression
  | WgslAstLogicExpression
  | WgslAstAddressOf
  | WgslAstIndirection
  | WgslAstIdentifier
  | WgslAstFunctionCall
  | WgslAstArrayAccess
  | WgslAstMemeberAccess
  | WgslAstSwizzeAccess
  | WgslAstGlobalConstDeclaration
  | WgslAstGlobalVariableDeclaration
  | WgslAstOverrideDeclaration
  | WgslAstVariableDeclaration
  | WgslAstConstDeclaration
  | WgslAstLetDeclaration
  | WgslAstComment
  | WgslAstUnknown;

interface BaseWgslAstNode {
  offsets?: AstOffsets;
}

export interface WgslAstProgramm extends BaseWgslAstNode {
  ast: WgslAstTypes.Programm;
  directives: WgslAstNode[];
  declarations: WgslAstNode[];
}

export interface WgslAstFunctionDeclaration extends BaseWgslAstNode {
  ast: WgslAstTypes.FunctionDeclaration;
  function_attributes: WgslAstNode[];
  name: AstToken;
  params: WgslAstNode[];
  return_type?: AstToken;
  return_attributes?: WgslAstNode[];
  return_template_list?: WgslAstNode;
  body: WgslAstNode;
}

export interface WgslAstTypeDefinition extends BaseWgslAstNode {
  ast: WgslAstTypes.TypeDefinition;
  type: AstToken;
  template_list?: WgslAstNode;
}

export interface WgslAstFunctionParam extends BaseWgslAstNode {
  ast: WgslAstTypes.FunctionParam;
  attributes: WgslAstNode[];
  name: AstToken;
  type?: WgslAstNode;
}

export interface WgslAstDiagnosticDirective extends BaseWgslAstNode {
  ast: WgslAstTypes.DiagnosticDirective;
  severity: AstToken;
  name: AstToken;
  dot_name?: AstToken;
  optional_comma: boolean;
}

export interface WgslAstEnableDirective extends BaseWgslAstNode {
  ast: WgslAstTypes.EnableDirective;
  extension_names: AstToken[];
  optional_comma: boolean;
}

export interface WgslAstRequiresDirective extends BaseWgslAstNode {
  ast: WgslAstTypes.RequiresDirective;
  extension_names: AstToken[];
  optional_comma: boolean;
}

export interface WgslAstDiagnosticAttribute extends BaseWgslAstNode {
  ast: WgslAstTypes.DiagnosticAttribute;
  severity: AstToken;
  name: AstToken;
  dot_name?: AstToken;
  optional_comma: boolean;
}

export interface WgslAstInterpolateAttribute extends BaseWgslAstNode {
  ast: WgslAstTypes.InterpolateAttribute;
  type: WgslAstNode;
  sampling?: WgslAstNode;
  optional_comma: boolean;
}

export interface WgslAstWorkgroupSizeAttribute extends BaseWgslAstNode {
  ast: WgslAstTypes.WorkgroupSizeAttribute;
  x: WgslAstNode;
  y?: WgslAstNode;
  z?: WgslAstNode;
  optional_comma: boolean;
}

export interface WgslAstConstAttribute extends BaseWgslAstNode {
  ast: WgslAstTypes.ConstAttribute;
}

export interface WgslAstInvariantAttribute extends BaseWgslAstNode {
  ast: WgslAstTypes.InvariantAttribute;
}

export interface WgslAstMustUseAttribute extends BaseWgslAstNode {
  ast: WgslAstTypes.MustUseAttribute;
}

export interface WgslAstVertexAttribute extends BaseWgslAstNode {
  ast: WgslAstTypes.VertexAttribute;
}

export interface WgslAstFragmentAttribute extends BaseWgslAstNode {
  ast: WgslAstTypes.FragmentAttribute;
}

export interface WgslAstComputeAttribute extends BaseWgslAstNode {
  ast: WgslAstTypes.ComputeAttribute;
}

export interface WgslAstAlignAttribute extends BaseWgslAstNode {
  ast: WgslAstTypes.AlignAttribute;
  size: WgslAstNode;
  optional_comma: boolean;
}

export interface WgslAstBindingAttribute extends BaseWgslAstNode {
  ast: WgslAstTypes.BindingAttribute;
  number: WgslAstNode;
  optional_comma: boolean;
}

export interface WgslAstBuiltinAttribute extends BaseWgslAstNode {
  ast: WgslAstTypes.BuiltinAttribute;
  associated_object: WgslAstNode;
  optional_comma: boolean;
}

export interface WgslAstGroupAttribute extends BaseWgslAstNode {
  ast: WgslAstTypes.GroupAttribute;
  group: WgslAstNode;
  optional_comma: boolean;
}

export interface WgslAstIdAttribute extends BaseWgslAstNode {
  ast: WgslAstTypes.IdAttribute;
  identifier: WgslAstNode;
  optional_comma: boolean;
}

export interface WgslAstLocationAttribute extends BaseWgslAstNode {
  ast: WgslAstTypes.LocationAttribute;
  part: WgslAstNode;
  optional_comma: boolean;
}

export interface WgslAstSizeAttribute extends BaseWgslAstNode {
  ast: WgslAstTypes.SizeAttribute;
  size: WgslAstNode;
  optional_comma: boolean;
}

export interface WgslAstStructDeclaration extends BaseWgslAstNode {
  ast: WgslAstTypes.StructDeclaration;
  name: AstToken;
  members: WgslAstNode[];
  optional_comma: boolean;
}

export interface WgslAstStructMember extends BaseWgslAstNode {
  ast: WgslAstTypes.StructMember;
  name: AstToken;
  type: WgslAstNode;
  attributes: WgslAstNode[];
}

export interface WgslAstTypeAlias extends BaseWgslAstNode {
  ast: WgslAstTypes.TypeAlias;
  name: AstToken;
  type: AstToken;
  template_list?: WgslAstNode;
}

export interface WgslAstCompoundStatement extends BaseWgslAstNode {
  ast: WgslAstTypes.CompoundStatement;
  attributes: WgslAstNode[];
  statements: WgslAstNode[];
}

export interface WgslAstStatement extends BaseWgslAstNode {
  ast: WgslAstTypes.Statement;
  statement: WgslAstNode;
}

export interface WgslAstEmptyDeclaration extends BaseWgslAstNode {
  ast: WgslAstTypes.EmptyDeclaration;
}

export interface WgslAstTemplateList extends BaseWgslAstNode {
  ast: WgslAstTypes.TemplateList;
  members: WgslAstNode[];
  optional_comma: boolean;
}

export interface WgslAstBreak extends BaseWgslAstNode {
  ast: WgslAstTypes.Break;
}

export interface WgslAstBreakIf extends BaseWgslAstNode {
  ast: WgslAstTypes.BreakIf;
  condition: WgslAstNode;
}

export interface WgslAstContinue extends BaseWgslAstNode {
  ast: WgslAstTypes.Continue;
}

export interface WgslAstContinuing extends BaseWgslAstNode {
  ast: WgslAstTypes.Continuing;
  attributes: WgslAstNode[];
  body: WgslAstNode[];
  break_if?: WgslAstBreakIf;
}

export interface WgslAstReturn extends BaseWgslAstNode {
  ast: WgslAstTypes.Return;
  data?: WgslAstNode;
}

export interface WgslAstConstAssert extends BaseWgslAstNode {
  ast: WgslAstTypes.ConstAssert;
  expression: WgslAstNode;
}

export interface WgslAstDiscard extends BaseWgslAstNode {
  ast: WgslAstTypes.Discard;
}

export interface WgslAstWhile extends BaseWgslAstNode {
  ast: WgslAstTypes.While;
  attributes: WgslAstNode[];
  condition: WgslAstNode;
  body: WgslAstNode;
}

export interface WgslAstFor extends BaseWgslAstNode {
  ast: WgslAstTypes.For;
  initializer?: WgslAstNode;
  condition?: WgslAstNode;
  update_part?: WgslAstNode;
  body: WgslAstNode;
}

export interface WgslAstLoop extends BaseWgslAstNode {
  ast: WgslAstTypes.Loop;
  attributes: WgslAstNode[];
  body_attributes: WgslAstNode[];
  body: WgslAstNode[];
  continuing?: WgslAstContinuing;
}

export interface WgslAstSwitch extends BaseWgslAstNode {
  ast: WgslAstTypes.Switch;
  attributes: WgslAstNode[];
  body_attributes: WgslAstNode[];
  condition: WgslAstNode;
  cases: WgslAstNode[];
}
export interface WgslAstSwitchCase extends BaseWgslAstNode {
  ast: WgslAstTypes.SwitchCase;
  selectors: WgslAstNode[];
  body: WgslAstNode;
}
export interface WgslAstSwitchDefault extends BaseWgslAstNode {
  ast: WgslAstTypes.SwitchDefault;
  body?: WgslAstNode;
}
export interface WgslAstIf extends BaseWgslAstNode {
  ast: WgslAstTypes.If;
  attributes: WgslAstNode[];
  clause: WgslAstIfClause;
  else_if_clauses: WgslAstIfClause[];
  else_clause?: WgslAstElseClause;
}

export interface WgslAstIfClause extends BaseWgslAstNode {
  ast: WgslAstTypes.IfClause;
  condition: WgslAstNode;
  body: WgslAstNode;
}
export interface WgslAstElseClause extends BaseWgslAstNode {
  ast: WgslAstTypes.ElseClause;
  body: WgslAstNode;
}

export interface WgslAstIncrement extends BaseWgslAstNode {
  ast: WgslAstTypes.Increment;
  lhs: WgslAstNode;
}
export interface WgslAstDecrement extends BaseWgslAstNode {
  ast: WgslAstTypes.Decrement;
  lhs: WgslAstNode;
}

export interface WgslAstAssignment extends BaseWgslAstNode {
  ast: WgslAstTypes.Assignment;
  lhs: WgslAstNode;
  operator: AstToken;
  value: WgslAstNode;
}
export interface WgslAstPhonyAssignment extends BaseWgslAstNode {
  ast: WgslAstTypes.PhonyAssignment;
  value: WgslAstNode;
}

export interface WgslAstBooleanLiteral extends BaseWgslAstNode {
  ast: WgslAstTypes.BooleanLiteral;
  value: boolean;
}

export interface WgslAstIntegerLiteral extends BaseWgslAstNode {
  ast: WgslAstTypes.IntegerLiteral;
  value: AstToken;
}

export interface WgslAstFloatLiteral extends BaseWgslAstNode {
  ast: WgslAstTypes.FloatLiteral;
  value: AstToken;
}

export interface WgslAstUnaryExpression extends BaseWgslAstNode {
  ast: WgslAstTypes.UnaryExpression;
  operator: AstToken;
  rhs: WgslAstNode;
}

export interface WgslAstParentesisExpression extends BaseWgslAstNode {
  ast: WgslAstTypes.ParentesisExpression;
  expression: WgslAstNode;
  specifier?: WgslAstNode;
}

export interface WgslAstAdditiveExpression extends BaseWgslAstNode {
  ast: WgslAstTypes.AdditiveExpression;
  lhs: WgslAstNode;
  operator: AstToken;
  rhs: WgslAstNode;
}

export interface WgslAstMultiplicativeExpression extends BaseWgslAstNode {
  ast: WgslAstTypes.MultiplicativeExpression;
  lhs: WgslAstNode;
  operator: AstToken;
  rhs: WgslAstNode;
}

export interface WgslAstBinaryExpression extends BaseWgslAstNode {
  ast: WgslAstTypes.BinaryExpression;
  lhs: WgslAstNode;
  operator: AstToken;
  rhs: WgslAstNode;
}

export interface WgslAstShiftExpression extends BaseWgslAstNode {
  ast: WgslAstTypes.ShiftExpression;
  lhs: WgslAstNode;
  operator: AstToken;
  rhs: WgslAstNode;
}

export interface WgslAstRelationalExpression extends BaseWgslAstNode {
  ast: WgslAstTypes.RelationalExpression;
  lhs: WgslAstNode;
  operator: AstToken;
  rhs: WgslAstNode;
}

export interface WgslAstLogicExpression extends BaseWgslAstNode {
  ast: WgslAstTypes.LogicExpression;
  lhs: WgslAstNode;
  operator: AstToken;
  rhs: WgslAstNode;
}

export interface WgslAstAddressOf extends BaseWgslAstNode {
  ast: WgslAstTypes.AddressOf;
  reference: WgslAstNode;
}
export interface WgslAstIndirection extends BaseWgslAstNode {
  ast: WgslAstTypes.Indirection;
  pointer: WgslAstNode;
}

export interface WgslAstIdentifier extends BaseWgslAstNode {
  ast: WgslAstTypes.Identifier;
  name: AstToken;
  template_list?: WgslAstNode;
  specifier?: WgslAstNode;
}
export interface WgslAstFunctionCall extends BaseWgslAstNode {
  ast: WgslAstTypes.FunctionCall;
  name: AstToken;
  template_list?: WgslAstNode;
  params: WgslAstNode[];
}

export interface WgslAstArrayAccess extends BaseWgslAstNode {
  ast: WgslAstTypes.ArrayAccess;
  index: WgslAstNode;
  specifier?: WgslAstNode;
}
export interface WgslAstMemeberAccess extends BaseWgslAstNode {
  ast: WgslAstTypes.MemeberAccess;
  name: AstToken;
  specifier?: WgslAstNode;
}
export interface WgslAstSwizzeAccess extends BaseWgslAstNode {
  ast: WgslAstTypes.SwizzeAccess;
  name: AstToken;
  specifier?: WgslAstNode;
}

export interface WgslAstGlobalConstDeclaration extends BaseWgslAstNode {
  ast: WgslAstTypes.GlobalConstDeclaration;
  declaration: WgslAstNode;
}
export interface WgslAstOverrideDeclaration extends BaseWgslAstNode {
  ast: WgslAstTypes.OverrideDeclaration;
  attributes: WgslAstNode[];
  name: AstToken;
  type?: WgslAstNode;
  value?: WgslAstNode;
}

export interface WgslAstConstDeclaration extends BaseWgslAstNode {
  ast: WgslAstTypes.ConstDeclaration;
  name: AstToken;
  type?: WgslAstNode;
  value: WgslAstNode;
}

export interface WgslAstLetDeclaration extends BaseWgslAstNode {
  ast: WgslAstTypes.LetDeclaration;
  name: AstToken;
  type?: WgslAstNode;
  value: WgslAstNode;
}

export interface WgslAstVariableDeclaration extends BaseWgslAstNode {
  ast: WgslAstTypes.VariableDeclaration;
  template_list?: WgslAstNode;
  name: AstToken;
  type?: WgslAstNode;
  value?: WgslAstNode;
}
export interface WgslAstGlobalVariableDeclaration extends BaseWgslAstNode {
  ast: WgslAstTypes.GlobalVariableDeclaration;
  declaration: WgslAstNode;
  attributes: WgslAstNode[];
}

export interface WgslAstComment extends BaseWgslAstNode {
  ast: WgslAstTypes.Comment;
  text: AstToken;
}

export interface WgslAstUnknown extends BaseWgslAstNode {
  ast: WgslAstTypes.Unknown;
}
