import { BaseWGSLCstVisitor } from './parser';
import * as cst from './cst.types';
import {
  WgslAstParams,
  WgslAstNode,
  WgslAstTypes,
  AstToken,
  WgslAstIfClause,
  WgslAstContinuing,
  WgslAstElseClause,
  WgslAstBreakIf,
} from './ast.types';
import {
  AllignAttr,
  BindingAttr,
  BuiltinAttr,
  ComputeAttr,
  ConstAttr,
  FragmentAttr,
  GroupAttr,
  IdAttr,
  InvariantAttr,
  LocationAttr,
  MustUseAttr,
  SizeAttr,
  VertexAttr,
} from './tokens';
import { IToken } from 'chevrotain';

function createAstToken(token: IToken): AstToken {
  return {
    text: token.image,
    start: token.startOffset,
    end: token.startOffset + token.image.length,
  };
}

export class WgslAstVisitor
  extends BaseWGSLCstVisitor
  implements cst.ICstNodeVisitor<WgslAstParams, WgslAstNode>
{
  programm(
    children: cst.ProgrammCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.Programm,
      directives: (children.global_directive ?? []).map((directive) =>
        this.visit(directive, param)
      ),
      declarations: (children.global_declaration ?? []).map((declaration) =>
        this.visit(declaration, param)
      ),
    };
  }

  global_directive(
    children: cst.Global_directiveCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    if (children.diagnostic_directive) {
      return this.visit(children.diagnostic_directive, param);
    } else if (children.enable_directive) {
      return this.visit(children.enable_directive, param);
    } else if (children.requires_directive) {
      return this.visit(children.requires_directive, param);
    } else if (children.Comment) {
      return {
        ast: WgslAstTypes.Comment,
        text: createAstToken(children.Comment[0]),
      };
    }
    return { ast: WgslAstTypes.Unknown };
  }

  diagnostic_directive(
    children: cst.Diagnostic_directiveCstChildren
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.DiagnosticDirective,
      severity: createAstToken(children.SeverityControlName[0]),
      name: createAstToken(children.diagnostic_name[0]),
      dot_name:
        children.diagnostic_dot_name &&
        createAstToken(children.diagnostic_dot_name[0]),
      optional_comma: Boolean(children.optional_comma),
    };
  }

  enable_directive(children: cst.Enable_directiveCstChildren): WgslAstNode {
    return {
      ast: WgslAstTypes.EnableDirective,
      extension_names: children.extension_name.map(createAstToken),
      optional_comma: Boolean(children.optional_comma),
    };
  }

  requires_directive(children: cst.Requires_directiveCstChildren): WgslAstNode {
    return {
      ast: WgslAstTypes.RequiresDirective,
      extension_names: children.extension_name.map(createAstToken),
      optional_comma: Boolean(children.optional_comma),
    };
  }

  global_declaration(
    children: cst.Global_declarationCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    if (children.const_declaration) {
      return {
        ast: WgslAstTypes.GlobalConstDeclaration,
        declaration: this.visit(children.const_declaration, param),
      };
    } else if (children.Semi) {
      return {
        ast: WgslAstTypes.EmptyDeclaration,
      };
    } else if (children.type_alias_declaration) {
      return this.visit(children.type_alias_declaration, param);
    } else if (children.const_assert_statement) {
      return this.visit(children.const_assert_statement, param);
    } else if (children.struct_declaration) {
      return this.visit(children.struct_declaration, param);
    } else if (children.override_declarations) {
      return this.visit(children.override_declarations, param);
    } else if (children.function_declaration) {
      return this.visit(children.function_declaration, param);
    } else if (children.global_variable_declaration) {
      return this.visit(children.global_variable_declaration, param);
    } else if (children.Comment) {
      return {
        ast: WgslAstTypes.Comment,
        text: createAstToken(children.Comment[0]),
      };
    }

    return { ast: WgslAstTypes.Unknown };
  }

  template_list(
    children: cst.Template_listCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.TemplateList,
      members: children.expression.map((member) => this.visit(member, param)),
      optional_comma: Boolean(children.optional_comma),
    };
  }

  type_definition(
    children: cst.Type_definitionCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.TypeDefinition,
      type: createAstToken(children.type[0]),
      template_list:
        children.template_list && this.visit(children.template_list, param),
    };
  }

  attribute(
    children: cst.AttributeCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    if (children.attribute_single_arg) {
      return this.visit(children.attribute_single_arg, param);
    } else if (children.attribute_no_args) {
      return this.visit(children.attribute_no_args, param);
    } else if (children.diagnostic_attribute) {
      return this.visit(children.diagnostic_attribute, param);
    } else if (children.interpolate_attribute) {
      return this.visit(children.interpolate_attribute, param);
    } else if (children.workgroup_size_attribute) {
      return this.visit(children.workgroup_size_attribute, param);
    }
    return { ast: WgslAstTypes.Unknown };
  }

  diagnostic_attribute(
    children: cst.Diagnostic_attributeCstChildren
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.DiagnosticAttribute,
      severity: createAstToken(children.SeverityControlName[0]),
      name: createAstToken(children.diagnostic_name[0]),
      dot_name:
        children.diagnostic_dot_name &&
        createAstToken(children.diagnostic_dot_name[0]),
      optional_comma: Boolean(children.optional_comma),
    };
  }

  interpolate_attribute(
    children: cst.Interpolate_attributeCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.InterpolateAttribute,
      type: this.visit(children.type, param),
      sampling: children.sampling && this.visit(children.sampling, param),
      optional_comma: Boolean(children.optional_comma),
    };
  }

  workgroup_size_attribute(
    children: cst.Workgroup_size_attributeCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.WorkgroupSizeAttribute,
      x: this.visit(children.x, param),
      y: children.y && this.visit(children.y, param),
      z: children.z && this.visit(children.z, param),
      optional_comma: Boolean(children.optional_comma),
    };
  }

  attribute_no_args(children: cst.Attribute_no_argsCstChildren): WgslAstNode {
    const [token] = children.AttributeNoParam;
    if (token.tokenType === ConstAttr) {
      return { ast: WgslAstTypes.ConstAttribute };
    } else if (token.tokenType === InvariantAttr) {
      return { ast: WgslAstTypes.InvariantAttribute };
    } else if (token.tokenType === MustUseAttr) {
      return { ast: WgslAstTypes.MustUseAttribute };
    } else if (token.tokenType === VertexAttr) {
      return { ast: WgslAstTypes.VertexAttribute };
    } else if (token.tokenType === FragmentAttr) {
      return { ast: WgslAstTypes.FragmentAttribute };
    } else if (token.tokenType === ComputeAttr) {
      return { ast: WgslAstTypes.ComputeAttribute };
    }
    return { ast: WgslAstTypes.Unknown };
  }

  attribute_single_arg(
    children: cst.Attribute_single_argCstChildren
  ): WgslAstNode {
    const [token] = children.AttributeSingleParam;
    const expression = this.visit(children.expression);
    const optional_comma = Boolean(children.optional_comma);

    if (token.tokenType === AllignAttr) {
      return {
        ast: WgslAstTypes.AlignAttribute,
        size: expression,
        optional_comma,
      };
    } else if (token.tokenType === BindingAttr) {
      return {
        ast: WgslAstTypes.BindingAttribute,
        number: expression,
        optional_comma,
      };
    } else if (token.tokenType === BuiltinAttr) {
      return {
        ast: WgslAstTypes.BuiltinAttribute,
        associated_object: expression,
        optional_comma,
      };
    } else if (token.tokenType === GroupAttr) {
      return {
        ast: WgslAstTypes.GroupAttribute,
        group: expression,
        optional_comma,
      };
    } else if (token.tokenType === IdAttr) {
      return {
        ast: WgslAstTypes.IdAttribute,
        identifier: expression,
        optional_comma,
      };
    } else if (token.tokenType === LocationAttr) {
      return {
        ast: WgslAstTypes.LocationAttribute,
        part: expression,
        optional_comma,
      };
    } else if (token.tokenType === SizeAttr) {
      return {
        ast: WgslAstTypes.SizeAttribute,
        size: expression,
        optional_comma,
      };
    }
    return { ast: WgslAstTypes.Unknown };
  }

  struct_declaration(
    children: cst.Struct_declarationCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.StructDeclaration,
      name: createAstToken(children.name[0]),
      members: children.struct_member.map((member) =>
        this.visit(member, param)
      ),
      optional_comma: Boolean(children.optional_comma),
    };
  }

  struct_member(
    children: cst.Struct_memberCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.StructMember,
      name: createAstToken(children.name[0]),
      type: this.visit(children.type_definition, param),
      attributes: (children.attribute ?? []).map((attribute) =>
        this.visit(attribute, param)
      ),
    };
  }

  type_alias_declaration(
    children: cst.Type_alias_declarationCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.TypeAlias,
      name: createAstToken(children.name[0]),
      type: createAstToken(children.type[0]),
      template_list:
        children.template_list && this.visit(children.template_list, param),
    };
  }

  variable_or_value_statement(
    children: cst.Variable_or_value_statementCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    if (children.const_declaration) {
      return this.visit(children.const_declaration, param);
    } else if (children.let_declaration) {
      return this.visit(children.let_declaration, param);
    } else if (children.variable_declaration) {
      return this.visit(children.variable_declaration, param);
    }

    return { ast: WgslAstTypes.Unknown };
  }

  variable_declaration(
    children: cst.Variable_declarationCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.VariableDeclaration,
      name: createAstToken(children.name[0]),
      type:
        children.type_definition && this.visit(children.type_definition, param),
      template_list:
        children.template_list && this.visit(children.template_list, param),
      value: children.expression && this.visit(children.expression, param),
    };
  }

  let_declaration(
    children: cst.Let_declarationCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.LetDeclaration,
      name: createAstToken(children.name[0]),
      type:
        children.type_definition && this.visit(children.type_definition, param),
      value: this.visit(children.expression, param),
    };
  }

  const_declaration(
    children: cst.Const_declarationCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.ConstDeclaration,
      name: createAstToken(children.name[0]),
      type:
        children.type_definition && this.visit(children.type_definition, param),
      value: this.visit(children.expression, param),
    };
  }

  override_declarations(
    children: cst.Override_declarationsCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.OverrideDeclaration,
      name: createAstToken(children.name[0]),
      attributes: (children.attribute ?? []).map((attr) =>
        this.visit(attr, param)
      ),
      type:
        children.type_definition && this.visit(children.type_definition, param),
      value: children.expression && this.visit(children.expression, param),
    };
  }

  global_variable_declaration(
    children: cst.Global_variable_declarationCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.GlobalVariableDeclaration,
      declaration: this.visit(children.variable_declaration),
      attributes: (children.attribute ?? []).map((attribute) =>
        this.visit(attribute, param)
      ),
    };
  }

  expression(
    children: cst.ExpressionCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    return this.visit(children.short_circuit_expression, param);
  }

  paren_expression(
    children: cst.Paren_expressionCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.ParentesisExpression,
      expression: this.visit(children.expression, param),
    };
  }

  ident_access(
    children: cst.Ident_accessCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.Identifier,
      name: createAstToken(children.identifier[0]),
      template_list:
        children.template_list && this.visit(children.template_list, param),
      specifier:
        children.component_or_swizzle_specifier &&
        this.visit(children.component_or_swizzle_specifier, param),
    };
  }

  component_or_swizzle_specifier(
    children: cst.Component_or_swizzle_specifierCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    const specifier =
      children.component_or_swizzle_specifier &&
      this.visit(children.component_or_swizzle_specifier, param);
    if (children.expression) {
      return {
        ast: WgslAstTypes.ArrayAccess,
        index: this.visit(children.expression, param),
        specifier,
      };
    } else if (children.member_ident) {
      return {
        ast: WgslAstTypes.MemeberAccess,
        name: createAstToken(children.member_ident[0]),
        specifier,
      };
    } else if (children.SwizzleName) {
      return {
        ast: WgslAstTypes.SwizzeAccess,
        name: createAstToken(children.SwizzleName[0]),
        specifier,
      };
    }

    return { ast: WgslAstTypes.Unknown };
  }

  call_phrase(
    children: cst.Call_phraseCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.FunctionCall,
      name: createAstToken(children.identifier[0]),
      template_list:
        children.template_list && this.visit(children.template_list, param),
      params: children.expression.map((p) => this.visit(p, param)),
    };
  }

  short_circuit_expression(
    children: cst.Short_circuit_expressionCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    if (children.LogicOperator) {
      const ops = children.LogicOperator;
      let index = 1;
      let result = this.visit(children.relational_expression, param);
      while (index < children.relational_expression.length) {
        const operator = createAstToken(ops[index - 1]);
        const rhs = this.visit(children.relational_expression[index], param);
        result = {
          ast: WgslAstTypes.RelationalExpression,
          lhs: result,
          operator,
          rhs,
        };
        index++;
      }
      return result;
    }
    return this.visit(children.relational_expression, param);
  }

  relational_expression(
    children: cst.Relational_expressionCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    if (children.RelationalOperator) {
      const ops = children.RelationalOperator;
      let index = 1;
      let result = this.visit(children.shift_expression, param);
      while (index < children.shift_expression.length) {
        const operator = createAstToken(ops[index - 1]);
        const rhs = this.visit(children.shift_expression[index], param);
        result = {
          ast: WgslAstTypes.RelationalExpression,
          lhs: result,
          operator,
          rhs,
        };
        index++;
      }
      return result;
    }
    return this.visit(children.shift_expression, param);
  }

  shift_expression(
    children: cst.Shift_expressionCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    if (children.ShiftOperator) {
      const ops = children.ShiftOperator;
      let index = 1;
      let result = this.visit(children.additive_expression, param);
      while (index < children.additive_expression.length) {
        const operator = createAstToken(ops[index - 1]);
        const rhs = this.visit(children.additive_expression[index], param);
        result = {
          ast: WgslAstTypes.ShiftExpression,
          lhs: result,
          operator,
          rhs,
        };
        index++;
      }
      return result;
    }
    return this.visit(children.additive_expression, param);
  }

  additive_expression(
    children: cst.Additive_expressionCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    if (children.AdditiveOperator) {
      const ops = children.AdditiveOperator;
      let index = 1;
      let result = this.visit(children.multiplicative_expression, param);
      while (index < children.multiplicative_expression.length) {
        const operator = createAstToken(ops[index - 1]);
        const rhs = this.visit(
          children.multiplicative_expression[index],
          param
        );
        result = {
          ast: WgslAstTypes.AdditiveExpression,
          lhs: result,
          operator,
          rhs,
        };
        index++;
      }
      return result;
    }
    return this.visit(children.multiplicative_expression, param);
  }

  multiplicative_expression(
    children: cst.Multiplicative_expressionCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    if (children.MultiplicativeOperator) {
      const ops = children.MultiplicativeOperator;
      let index = 1;
      let result = this.visit(children.binary_expression, param);
      while (index < children.binary_expression.length) {
        const operator = createAstToken(ops[index - 1]);
        const rhs = this.visit(children.binary_expression[index], param);
        result = {
          ast: WgslAstTypes.MultiplicativeExpression,
          lhs: result,
          operator,
          rhs,
        };
        index++;
      }
      return result;
    }
    return this.visit(children.binary_expression, param);
  }

  binary_expression(
    children: cst.Binary_expressionCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    if (children.BinaryOperator) {
      const ops = children.BinaryOperator;
      let index = 1;
      let result = this.visit(children.unary_expression, param);
      while (index < children.unary_expression.length) {
        const operator = createAstToken(ops[index - 1]);
        const rhs = this.visit(children.unary_expression[index], param);
        result = {
          ast: WgslAstTypes.BinaryExpression,
          lhs: result,
          operator,
          rhs,
        };
        index++;
      }
      return result;
    }
    return this.visit(children.unary_expression, param);
  }

  unary_expression(
    children: cst.Unary_expressionCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    if (children.UnaryOperator) {
      return {
        ast: WgslAstTypes.UnaryExpression,
        operator: createAstToken(children.UnaryOperator[0]),
        rhs: this.visit(children.atom, param),
      };
    }
    return this.visit(children.atom, param);
  }

  atom(
    children: cst.AtomCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    if (children.call_phrase) {
      return this.visit(children.call_phrase, param);
    } else if (children.ident_access) {
      return this.visit(children.ident_access, param);
    } else if (children.literal) {
      return this.visit(children.literal, param);
    } else if (children.paren_expression) {
      return this.visit(children.paren_expression, param);
    }
    return { ast: WgslAstTypes.Unknown };
  }

  lhs_expression(
    children: cst.Lhs_expressionCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    if (children.Amp && children.lhs_expression) {
      return {
        ast: WgslAstTypes.AddressOf,
        reference: this.visit(children.lhs_expression, param),
      };
    } else if (children.Mult && children.lhs_expression) {
      return {
        ast: WgslAstTypes.Indirection,
        pointer: this.visit(children.lhs_expression, param),
      };
    } else if (children.lhs_expression_ident) {
      return this.visit(children.lhs_expression_ident, param);
    } else if (children.lhs_expression_parentesis) {
      return this.visit(children.lhs_expression_parentesis, param);
    }
    return { ast: WgslAstTypes.Unknown };
  }

  lhs_expression_ident(
    children: cst.Lhs_expression_identCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.Identifier,
      name: createAstToken(children.name[0]),
      specifier:
        children.component_or_swizzle_specifier &&
        this.visit(children.component_or_swizzle_specifier, param),
    };
  }

  lhs_expression_parentesis(
    children: cst.Lhs_expression_parentesisCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.ParentesisExpression,
      expression: this.visit(children.lhs_expression, param),
      specifier:
        children.component_or_swizzle_specifier &&
        this.visit(children.component_or_swizzle_specifier, param),
    };
  }

  compound_statement(
    children: cst.Compound_statementCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.CompoundStatement,
      attributes: (children.attribute ?? []).map((attr) =>
        this.visit(attr, param)
      ),
      statements: (children.statement ?? []).map((statement) =>
        this.visit(statement, param)
      ),
    };
  }

  assignment_statement(
    children: cst.Assignment_statementCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    const lhs = this.visit(children.lhs_expression, param);
    if (children.PlusPlus) {
      return {
        ast: WgslAstTypes.Increment,
        lhs,
      };
    } else if (children.MinusMinus) {
      return {
        ast: WgslAstTypes.Decrement,
        lhs,
      };
    } else if (children.operator && children.expression) {
      return {
        ast: WgslAstTypes.Assignment,
        lhs,
        operator: createAstToken(children.operator[0]),
        value: this.visit(children.expression, param),
      };
    }
    return { ast: WgslAstTypes.Unknown };
  }

  phony_assignment(
    children: cst.Phony_assignmentCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.PhonyAssignment,
      value: this.visit(children.phony_value, param),
    };
  }

  if_statement(
    children: cst.If_statementCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.If,
      attributes: (children.attribute ?? []).map((attr) =>
        this.visit(attr, param)
      ),
      clause: this.visit(children.if_clause) as WgslAstIfClause,
      else_if_clauses: (children.else_if_clause ?? []).map((clause) =>
        this.visit(clause, param) as WgslAstIfClause
      ),
      else_clause: children.else_clause && this.visit(children.else_clause) as WgslAstElseClause,
    };
  }

  if_clause(
    children: cst.If_clauseCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.IfClause,
      condition: this.visit(children.expression, param),
      body: this.visit(children.compound_statement, param),
    };
  }

  else_if_clause(
    children: cst.Else_if_clauseCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.IfClause,
      condition: this.visit(children.expression, param),
      body: this.visit(children.compound_statement, param),
    };
  }

  else_clause(
    children: cst.Else_clauseCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.ElseClause,
      body: this.visit(children.compound_statement, param),
    };
  }

  switch_statement(
    children: cst.Switch_statementCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.Switch,
      attributes: (children.attribute ?? []).map((attr) =>
        this.visit(attr, param)
      ),
      body_attributes: (children.body_attribute ?? []).map((attr) =>
        this.visit(attr, param)
      ),
      condition: this.visit(children.expression, param),
      cases: (children.switch_clause ?? []).map((clause) =>
        this.visit(clause, param)
      ),
    };
  }

  switch_clause(
    children: cst.Switch_clauseCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    if (children.case_clause) {
      return this.visit(children.case_clause, param);
    } else if (children.default_alone_clause) {
      return this.visit(children.default_alone_clause, param);
    }
    return { ast: WgslAstTypes.Unknown };
  }

  case_clause(
    children: cst.Case_clauseCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    const selectors = children.case_selector.map((selector) =>
      this.visit(selector, param)
    );

    return {
      ast: WgslAstTypes.SwitchCase,
      selectors,
      body: this.visit(children.compound_statement, param),
    };
  }

  default_alone_clause(
    children: cst.Default_alone_clauseCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.SwitchDefault,
      body: this.visit(children.compound_statement, param),
    };
  }

  case_selector(
    children: cst.Case_selectorCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    if (children.expression) {
      return this.visit(children.expression, param);
    } else if (children.Default) {
      return {
        ast: WgslAstTypes.SwitchDefault,
      };
    }

    return { ast: WgslAstTypes.Unknown };
  }

  loop_statement(
    children: cst.Loop_statementCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.Loop,
      attributes: (children.attribute ?? []).map((attr) =>
        this.visit(attr, param)
      ),
      body_attributes: (children.body_attribute ?? []).map((attr) =>
        this.visit(attr, param)
      ),
      body: (children.statement ?? []).map((statement) =>
        this.visit(statement, param)
      ),
      continuing:
        children.continuing_statement &&
        this.visit(children.continuing_statement, param) as WgslAstContinuing,
    };
  }

  for_statement(
    children: cst.For_statementCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.For,
      initializer: children.for_init && this.visit(children.for_init, param),
      condition:
        children.for_condition && this.visit(children.for_condition, param),
      update_part:
        children.for_update && this.visit(children.for_update, param),
      body: this.visit(children.compound_statement, param),
    };
  }

  for_init(
    children: cst.For_initCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    if (children.call_phrase) {
      return this.visit(children.call_phrase, param);
    } else if (children.variable_or_value_statement) {
      return this.visit(children.variable_or_value_statement, param);
    } else if (children.variable_updating_statement) {
      return this.visit(children.variable_updating_statement, param);
    }

    return { ast: WgslAstTypes.Unknown };
  }

  for_update(
    children: cst.For_updateCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    if (children.call_phrase) {
      return this.visit(children.call_phrase, param);
    } else if (children.variable_updating_statement) {
      return this.visit(children.variable_updating_statement, param);
    }
    return { ast: WgslAstTypes.Unknown };
  }

  while_statement(
    children: cst.While_statementCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.While,
      condition: this.visit(children.expression, param),
      attributes: (children.attribute ?? []).map((attr) =>
        this.visit(attr, param)
      ),
      body: this.visit(children.compound_statement, param),
    };
  }
  break_statement(_children: cst.Break_statementCstChildren): WgslAstNode {
    return {
      ast: WgslAstTypes.Break,
    };
  }
  break_if_statement(
    children: cst.Break_if_statementCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.BreakIf,
      condition: this.visit(children.expression, param),
    };
  }
  continue_statement(
    _children: cst.Continue_statementCstChildren
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.Continue,
    };
  }

  discard_statement(_children: cst.Discard_statementCstChildren): WgslAstNode {
    return {
      ast: WgslAstTypes.Discard,
    };
  }

  continuing_statement(
    children: cst.Continuing_statementCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.Continuing,
      attributes: (children.attribute ?? []).map((attr) =>
        this.visit(attr, param)
      ),
      body: (children.statement ?? []).map((attr) => this.visit(attr, param)),
      break_if:
        children.break_if_statement &&
        this.visit(children.break_if_statement, param) as WgslAstBreakIf,
    };
  }

  return_statement(
    children: cst.Return_statementCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.Return,
      data: children.expression && this.visit(children.expression, param),
    };
  }

  const_assert_statement(
    children: cst.Const_assert_statementCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.ConstAssert,
      expression: this.visit(children.expression, param),
    };
  }

  statement(
    children: cst.StatementCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    if (children.compound_statement) {
      return this.visit(children.compound_statement, param);
    } else if (children.return_statement) {
      return this.visit(children.return_statement, param);
    } else if (children.break_statement) {
      return this.visit(children.break_statement, param);
    } else if (children.continue_statement) {
      return this.visit(children.continue_statement, param);
    } else if (children.discard_statement) {
      return this.visit(children.discard_statement, param);
    } else if (children.const_assert_statement) {
      return this.visit(children.const_assert_statement, param);
    } else if (children.if_statement) {
      return this.visit(children.if_statement, param);
    } else if (children.switch_statement) {
      return this.visit(children.switch_statement, param);
    } else if (children.loop_statement) {
      return this.visit(children.loop_statement, param);
    } else if (children.for_statement) {
      return this.visit(children.for_statement, param);
    } else if (children.while_statement) {
      return this.visit(children.while_statement, param);
    } else if (children.call_phrase) {
      return {
        ast: WgslAstTypes.Statement,
        statement: this.visit(children.call_phrase, param),
      };
    } else if (children.variable_or_value_statement) {
      return {
        ast: WgslAstTypes.Statement,
        statement: this.visit(children.variable_or_value_statement, param),
      };
    } else if (children.variable_updating_statement) {
      return {
        ast: WgslAstTypes.Statement,
        statement: this.visit(children.variable_updating_statement, param),
      };
    } else if (children.Semi) {
      return {
        ast: WgslAstTypes.EmptyDeclaration,
      };
    } else if (children.Comment) {
      return {
        ast: WgslAstTypes.Comment,
        text: createAstToken(children.Comment[0]),
      };
    }

    return { ast: WgslAstTypes.Unknown };
  }

  variable_updating_statement(
    children: cst.Variable_updating_statementCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    if (children.phony_assignment) {
      return this.visit(children.phony_assignment, param);
    } else if (children.assignment_statement) {
      return this.visit(children.assignment_statement, param);
    }

    return { ast: WgslAstTypes.Unknown };
  }

  function_declaration(
    children: cst.Function_declarationCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.FunctionDeclaration,
      name: createAstToken(children.name[0]),
      function_attributes: (children.function_attributes ?? []).map((attr) =>
        this.visit(attr, param)
      ),
      params: (children.param ?? []).map((p) => this.visit(p, param)),
      body: this.visit(children.compound_statement),
      return_type:
        children.return_type && createAstToken(children.return_type[0]),
      return_attributes: (children.return_attributes ?? []).map((attr) =>
        this.visit(attr, param)
      ),
      return_template_list:
        children.template_list && this.visit(children.template_list, param),
    };
  }

  param(
    children: cst.ParamCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    return {
      ast: WgslAstTypes.FunctionParam,
      name: createAstToken(children.name[0]),
      type:
        children.type_definition && this.visit(children.type_definition, param),
      attributes: (children.attribute ?? []).map((attribute) =>
        this.visit(attribute, param)
      ),
    };
  }

  int_literal(children: cst.Int_literalCstChildren): WgslAstNode {
    let token: AstToken = { text: '', start: -1, end: -1 };
    if (children.HexIntLiteral)
      token = createAstToken(children.HexIntLiteral[0]);
    if (children.DecimalIntLiteral)
      token = createAstToken(children.DecimalIntLiteral[0]);
    return {
      ast: WgslAstTypes.IntegerLiteral,
      value: token,
    };
  }
  float_literal(children: cst.Float_literalCstChildren): WgslAstNode {
    let token: AstToken = { text: '', start: -1, end: -1 };
    if (children.HexFloatLiteral)
      token = createAstToken(children.HexFloatLiteral[0]);
    if (children.DecimalFloatLiteral)
      token = createAstToken(children.DecimalFloatLiteral[0]);
    return {
      ast: WgslAstTypes.FloatLiteral,
      value: token,
    };
  }

  literal(
    children: cst.LiteralCstChildren,
    param?: WgslAstParams | undefined
  ): WgslAstNode {
    if (children.int_literal) {
      return this.visit(children.int_literal, param);
    } else if (children.float_literal) {
      return this.visit(children.float_literal, param);
    } else if (children.BooleanLiteral) {
      return {
        ast: WgslAstTypes.BooleanLiteral,
        value: children.BooleanLiteral[0].image === 'true',
      };
    }
    return { ast: WgslAstTypes.Unknown };
  }
}
