import { RemoteConsole } from 'vscode-languageserver/node';

export type LogLevel = 'error' | 'log' | 'debug';

export class Logger {
  private nested: Logger[] = [];
  constructor(
    private console: RemoteConsole,
    private level: LogLevel = 'log',
    private prefix = ''
  ) {}

  error(...args: unknown[]): void {
    this.console.error(this.prepareArgs(args));
  }
  warn(...args: unknown[]): void {
    if (this.level === 'error') return;
    this.console.warn(this.prepareArgs(args));
  }
  info(...args: unknown[]): void {
    if (this.level === 'error') return;
    this.console.info(this.prepareArgs(args));
  }

  log(...args: unknown[]): void {
    if (this.level === 'error') return;
    this.console.log(this.prepareArgs(args));
  }

  debug(...args: unknown[]): void {
    if (this.level !== 'debug') return;
    this.log('[Debug]', ...args);
  }

  changeLogLevel(level: LogLevel): void {
    this.level = level;
    for (const nested of this.nested) {
      nested.changeLogLevel(level);
    }
  }

  createdNested(prefix: string) {
    const nested = new Logger(
      this.console,
      this.level,
      this.prefix ? `${this.prefix} > ${prefix}` : prefix
    );
    this.nested.push(nested);
    return nested;
  }

  private prepareArgs(args: unknown[]): string {
    return [
      this.prefix && `[${this.prefix}]`,
      ...args.map((arg) =>
        typeof arg === 'object' ? JSON.stringify(arg) : String(arg)
      ),
    ]
      .filter(Boolean)
      .join(' ');
  }
}
