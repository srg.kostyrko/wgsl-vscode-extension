import { createConnection, ProposedFeatures } from 'vscode-languageserver/node';
import { ServerController } from './server-controller';

// Create a connection for the server, using Node's IPC as a transport.
// Also include all preview / proposed LSP features.
const connection = createConnection(ProposedFeatures.all);

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const controller = new ServerController(connection);
