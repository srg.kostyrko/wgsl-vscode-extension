import {
  SemanticTokensClientCapabilities,
  SemanticTokensLegend,
} from 'vscode-languageserver';

export enum TokenTypes {
  laguage_literal = 1,
  parameter,
  type,
  property,
  variable,
  function,
  _,
}

export enum TokenModifiers {
  declaration,
  readonly,
  deprecated,
  documentation,
  defaultLibrary,
  _,
}

const mapTokenTypes: Record<TokenTypes, string> = {
  [TokenTypes.laguage_literal]: 'enum	',
  [TokenTypes.parameter]: 'parameter',
  [TokenTypes.type]: 'type',
  [TokenTypes.property]: 'property',
  [TokenTypes.variable]: 'variable',
  [TokenTypes.function]: 'function',
  [TokenTypes._]: '',
};

export function computeLegend(
  capability?: SemanticTokensClientCapabilities
): SemanticTokensLegend {
  const clientTokenModifiers = new Set<string>(capability?.tokenModifiers);

  const tokenTypes: string[] = [];
  for (let i = 0; i < TokenTypes._; i++) {
    tokenTypes.push(mapTokenTypes[i as TokenTypes]);
  }

  const tokenModifiers: string[] = [];
  for (let i = 0; i < TokenModifiers._; i++) {
    const str = TokenModifiers[i];
    if (clientTokenModifiers.has(str)) {
      tokenModifiers.push(str);
    }
  }

  return { tokenTypes, tokenModifiers };
}
