import { Range } from 'vscode-languageserver/node';
import { TokenModifiers, TokenTypes } from './legend';

interface SemanticTokenData {
  line: number;
  char: number;
  length: number;
  type: number;
  mods: number;
}

export class SemanticTokensBuilder {
  private _prevLine = 0;
  private _prevChar = 0;
  private _data: number[] = [];
  private _dataLen = 0;

  private _tokens: SemanticTokenData[] = [];

  public pushBatch(
    ranges: Range[],
    tokenType: TokenTypes,
    tokenModifiers?: TokenModifiers[]
  ): void {
    for (const range of ranges) {
      this.push(range, tokenType, tokenModifiers);
    }
  }

  public push(
    range: Range,
    tokenType: TokenTypes,
    tokenModifiers?: TokenModifiers[]
  ): void {
    if (range.start.line !== range.end.line) {
      throw new Error('`range` cannot span multiple lines');
    }
    const line = range.start.line;
    const char = range.start.character;
    const length = range.end.character - range.start.character;
    let nTokenModifiers = 0;
    if (tokenModifiers) {
      for (const tokenModifier of tokenModifiers) {
        nTokenModifiers |= (1 << tokenModifier) >>> 0;
      }
    }
    this._tokens.push({
      line,
      char,
      length,
      type: tokenType,
      mods: nTokenModifiers,
    });
  }

  encode(token: SemanticTokenData): void {
    let pushLine = token.line;
    let pushChar = token.char;
    if (this._dataLen > 0) {
      pushLine -= this._prevLine;
      if (pushLine === 0) {
        pushChar -= this._prevChar;
      }
    }
    this._data[this._dataLen++] = pushLine;
    this._data[this._dataLen++] = pushChar;
    this._data[this._dataLen++] = token.length;
    this._data[this._dataLen++] = token.type;
    this._data[this._dataLen++] = token.mods;
    this._prevLine = token.line;
    this._prevChar = token.char;
  }

  public build(): { data: number[] } {
    this._tokens.sort(compare);
    for (const token of this._tokens) {
      this.encode(token);
    }
    return { data: [...new Uint32Array(this._data)] };
  }
}

function compare(a: SemanticTokenData, b: SemanticTokenData): number {
  if (a.line === b.line) {
    return a.char - b.char;
  }
  return a.line - b.line;
}
