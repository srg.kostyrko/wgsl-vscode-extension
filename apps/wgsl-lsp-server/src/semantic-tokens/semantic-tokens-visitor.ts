import { BaseWgslVisitor, ast } from '@wgsl/parser';
import { SemanticTokensBuilder } from './semantic-tokens-builder';
import { WGSLSourceDocument } from '../wgsl-source-document';
import { TokenModifiers, TokenTypes } from './legend';
import { getRangeForOffset } from '../misc';

interface SemanticTokensCtx {
  builder: SemanticTokensBuilder;
  document: WGSLSourceDocument;
}

export class SemanticTokensVisitor extends BaseWgslVisitor<SemanticTokensCtx> {
  run(
    node: ast.WgslAstNode,
    builder: SemanticTokensBuilder,
    document: WGSLSourceDocument
  ) {
    const ctx: SemanticTokensCtx = {
      builder,
      document,
    };

    this.visit(node, ctx);
  }

  visitDiagnosticDirective(
    node: ast.WgslAstDiagnosticDirective,
    ctx: SemanticTokensCtx
  ): void {
    ctx.builder.push(
      getRangeForOffset(ctx.document, node.severity.start, node.severity.end),
      TokenTypes.laguage_literal,
      []
    );
    ctx.builder.push(
      getRangeForOffset(ctx.document, node.name.start, node.name.end),
      TokenTypes.parameter,
      []
    );
    if (node.dot_name) {
      ctx.builder.push(
        getRangeForOffset(ctx.document, node.dot_name.start, node.dot_name.end),
        TokenTypes.parameter,
        []
      );
    }
  }

  visitEnableDirective(
    node: ast.WgslAstEnableDirective,
    ctx: SemanticTokensCtx
  ): void {
    for (const token of node.extension_names) {
      ctx.builder.push(
        getRangeForOffset(ctx.document, token.start, token.end),
        TokenTypes.parameter,
        []
      );
    }
  }

  visitRequiresDirective(
    node: ast.WgslAstRequiresDirective,
    ctx: SemanticTokensCtx
  ): void {
    for (const token of node.extension_names) {
      ctx.builder.push(
        getRangeForOffset(ctx.document, token.start, token.end),
        TokenTypes.parameter,
        []
      );
    }
  }

  visitTypeDefinition(
    node: ast.WgslAstTypeDefinition,
    ctx: SemanticTokensCtx
  ): void {
    ctx.builder.push(
      getRangeForOffset(ctx.document, node.type.start, node.type.end),
      TokenTypes.type,
      [TokenModifiers.declaration]
    );
    super.visitTypeDefinition(node, ctx);
  }

  visitStructDeclaration(
    node: ast.WgslAstStructDeclaration,
    ctx: SemanticTokensCtx
  ): void {
    ctx.builder.push(
      getRangeForOffset(ctx.document, node.name.start, node.name.end),
      TokenTypes.type,
      [TokenModifiers.declaration]
    );
    super.visitStructDeclaration(node, ctx);
  }

  visitStructMember(
    node: ast.WgslAstStructMember,
    ctx: SemanticTokensCtx
  ): void {
    ctx.builder.push(
      getRangeForOffset(ctx.document, node.name.start, node.name.end),
      TokenTypes.property,
      []
    );
    super.visitStructMember(node, ctx);
  }

  visitTypeAlias(node: ast.WgslAstTypeAlias, ctx: SemanticTokensCtx): void {
    ctx.builder.push(
      getRangeForOffset(ctx.document, node.name.start, node.name.end),
      TokenTypes.type,
      [TokenModifiers.declaration]
    );
    ctx.builder.push(
      getRangeForOffset(ctx.document, node.type.start, node.type.end),
      TokenTypes.type,
      []
    );
    super.visitTypeAlias(node, ctx);
  }

  visitVariableDeclaration(
    node: ast.WgslAstVariableDeclaration,
    ctx: SemanticTokensCtx
  ): void {
    ctx.builder.push(
      getRangeForOffset(ctx.document, node.name.start, node.name.end),
      TokenTypes.variable,
      [TokenModifiers.declaration]
    );
    super.visitVariableDeclaration(node, ctx);
  }

  visitFunctionDeclaration(
    node: ast.WgslAstFunctionDeclaration,
    ctx: SemanticTokensCtx
  ): void {
    ctx.builder.push(
      getRangeForOffset(ctx.document, node.name.start, node.name.end),
      TokenTypes.function,
      [TokenModifiers.declaration]
    );
    if (node.return_type) {
      ctx.builder.push(
        getRangeForOffset(
          ctx.document,
          node.return_type.start,
          node.return_type.end
        ),
        TokenTypes.type,
        []
      );
    }
    super.visitFunctionDeclaration(node, ctx);
  }

  visitFunctionParam(
    node: ast.WgslAstFunctionParam,
    ctx: SemanticTokensCtx
  ): void {
    ctx.builder.push(
      getRangeForOffset(ctx.document, node.name.start, node.name.end),
      TokenTypes.parameter,
      []
    );
    super.visitFunctionParam(node, ctx);
  }

  visitDiagnosticAttribute(
    node: ast.WgslAstDiagnosticAttribute,
    ctx: SemanticTokensCtx
  ): void {
    ctx.builder.push(
      getRangeForOffset(ctx.document, node.severity.start, node.severity.end),
      TokenTypes.laguage_literal,
      []
    );
    ctx.builder.push(
      getRangeForOffset(ctx.document, node.name.start, node.name.end),
      TokenTypes.parameter,
      []
    );
    if (node.dot_name) {
      ctx.builder.push(
        getRangeForOffset(ctx.document, node.dot_name.start, node.dot_name.end),
        TokenTypes.parameter,
        []
      );
    }
  }

  visitFunctionCall(
    node: ast.WgslAstFunctionCall,
    ctx: SemanticTokensCtx
  ): void {
    ctx.builder.push(
      getRangeForOffset(ctx.document, node.name.start, node.name.end),
      TokenTypes.function,
      []
    );
    super.visitFunctionCall(node, ctx);
  }

  visitMemeberAccess(
    node: ast.WgslAstMemeberAccess,
    ctx: SemanticTokensCtx
  ): void {
    ctx.builder.push(
      getRangeForOffset(ctx.document, node.name.start, node.name.end),
      TokenTypes.property,
      []
    );
    super.visitMemeberAccess(node, ctx);
  }

  visitSwizzeAccess(
    node: ast.WgslAstSwizzeAccess,
    ctx: SemanticTokensCtx
  ): void {
    ctx.builder.push(
      getRangeForOffset(ctx.document, node.name.start, node.name.end),
      TokenTypes.property,
      []
    );
    super.visitSwizzeAccess(node, ctx);
  }

  visitOverrideDeclaration(
    node: ast.WgslAstOverrideDeclaration,
    ctx: SemanticTokensCtx
  ): void {
    ctx.builder.push(
      getRangeForOffset(ctx.document, node.name.start, node.name.end),
      TokenTypes.variable,
      [TokenModifiers.declaration]
    );
    super.visitOverrideDeclaration(node, ctx);
  }

  visitLetDeclaration(
    node: ast.WgslAstLetDeclaration,
    ctx: SemanticTokensCtx
  ): void {
    ctx.builder.push(
      getRangeForOffset(ctx.document, node.name.start, node.name.end),
      TokenTypes.variable,
      [TokenModifiers.declaration]
    );
    super.visitLetDeclaration(node, ctx);
  }

  visitConstDeclaration(
    node: ast.WgslAstConstDeclaration,
    ctx: SemanticTokensCtx
  ): void {
    ctx.builder.push(
      getRangeForOffset(ctx.document, node.name.start, node.name.end),
      TokenTypes.variable,
      [TokenModifiers.declaration, TokenModifiers.readonly]
    );
    super.visitConstDeclaration(node, ctx);
  }

  visitIdentifier(node: ast.WgslAstIdentifier, ctx: SemanticTokensCtx): void {
    ctx.builder.push(
      getRangeForOffset(ctx.document, node.name.start, node.name.end),
      TokenTypes.variable,
      []
    );
    super.visitIdentifier(node, ctx);
  }
}
