export interface Defered<T> {
  promise: Promise<T>;
  resolve: (value: T) => void;
  reject: (reason: Error) => void;
  next(): Defered<T>;
}

export function defer<T>(): Defered<T> {
  const deferred: Defered<T> = {} as Defered<T>;
  const promise = new Promise<T>((resolve, reject) => {
    deferred.resolve = resolve;
    deferred.reject = reject;
  });
  deferred.promise = promise;
  deferred.next = () => {
    const next = defer<T>();
    next.promise
      .then((value) => deferred.resolve(value))
      .catch((err) => deferred.reject(err));
    return next;
  };
  return deferred;
}
