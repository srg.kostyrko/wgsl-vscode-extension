import { WgslAstVisitor, WgslLexer, WgslParser, ast, disambiguateTemplateLists } from '@wgsl/parser';
import {
  Diagnostic,
  DocumentUri,
  SemanticTokens,
  TextDocumentContentChangeEvent,
} from 'vscode-languageserver';
import { Signal } from './signal';
import { WGSLSourceDocument } from './wgsl-source-document';
import { nextTick } from './misc';
import { Scheduler } from './scheduler';
import { defer } from './defer';
import { CstNode, IToken } from 'chevrotain';
import { extractParseErrorRange } from './helpers';
import { SemanticTokensVisitor } from './semantic-tokens/semantic-tokens-visitor';
import { SemanticTokensBuilder } from './semantic-tokens/semantic-tokens-builder';

export class WGSLFileManager {
  public readonly diagnosticSignal: Signal<{
    uri: DocumentUri;
    diagnostics: Diagnostic[];
  }> = new Signal();
  public readonly destroyed: Signal<WGSLFileManager> = new Signal();

  #document: WGSLSourceDocument | null = null;

  private scheduler = new Scheduler(100);
  private isUpdating = false;

  private areTokensReady = defer<void>();
  private isReady = defer<void>();
  private isUpdated = defer<void>();

  private tokens: IToken[] = [];
  private cst: CstNode | null = null;
  private ast: ast.WgslAstNode | null = null;

  private diagnostics: Diagnostic[] = [];

  constructor(private parser: WgslParser, private astVisitor: WgslAstVisitor) {}

  get document(): WGSLSourceDocument | null {
    return this.#document;
  }

  getDocument(_uri: string): WGSLSourceDocument | null {
    return this.#document;
  }

  openDocument(_uri: string): void {
    //
  }
  addDocument(uri: string, version: number, content: string): void {
    if (this.#document) {
      this.updateDocument(uri, version, [{ text: content }]);
      return;
    }
    this.#document = new WGSLSourceDocument(uri, version, content);
    this.update();
  }

  updateDocument(
    _uri: string,
    version: number,
    changes: TextDocumentContentChangeEvent[]
  ): void {
    if (this.#document) {
      this.#document.update(version, changes);
      this.scheduler.schedule();
    }
  }

  closeDocument(_uri: string): void {
    this.dispose();
  }

  removeDocument(_uri: string): void {
    this.dispose();
  }

  private reportDiagnostics() {
    if (!this.#document) return;
    this.diagnosticSignal.trigger({
      uri: this.#document.uri,
      diagnostics: this.diagnostics,
    });
  }

  private update = async () => {
    if (this.isUpdating) {
      this.scheduler.schedule();
      return;
    }
    this.isUpdated = this.isUpdated.next();
    this.isUpdating = true;
    this.cleanup();

    this.tokenizeDocument();
    this.areTokensReady.resolve();

    await nextTick();

    this.parseDocument();

    await nextTick();

    this.buildAst();

    this.isReady.resolve();

    this.isUpdating = false;
    this.isUpdated.resolve();
  };

  private tokenizeDocument() {
    if (!this.#document) {
      this.tokens = [];
      return;
    }
    const result = WgslLexer.tokenize(this.#document.getText());
    this.tokens = disambiguateTemplateLists(result.tokens);
    for (const lexError of result.errors) {
      this.diagnostics.push({
        range: this.#document.rangeForOffsets(
          lexError.offset,
          lexError.offset + lexError.length
        ),
        message: lexError.message,
      });
    }
    this.reportDiagnostics();
  }

  private parseDocument() {
    if (!this.#document) {
      this.cst = null;
      return;
    }
    this.parser.input = this.tokens;
    this.cst = this.parser.programm();
    for (const parseError of this.parser.errors) {
      const [start, end] = extractParseErrorRange(parseError);
      this.diagnostics.push({
        range: this.#document.rangeForOffsets(start, end),
        message: parseError.message,
      });
    }
    this.reportDiagnostics();
  }

  private buildAst() {
    if (!this.#document || !this.cst) {
      this.ast = null;
      return;
    }
    this.ast = this.astVisitor.visit(this.cst);
    this.cst = null;
  }

  async getSemainticTokens(): Promise<SemanticTokens> {
    if (!this.#document) {
      return { data: [] };
    }
    const builder = new SemanticTokensBuilder();
    const semanticVisitor = new SemanticTokensVisitor();
    semanticVisitor.run(this.ast, builder, this.#document)
    return builder.build();
  }

  private cleanup() {
    this.tokens = [];
    this.cst = null;
    this.diagnostics = [];

    this.areTokensReady = this.areTokensReady.next();
    this.isReady = this.isReady.next();
  }

  dispose() {
    this.cleanup();
    this.reportDiagnostics();
    this.#document = null;
    this.destroyed.trigger(this);
  }
}
