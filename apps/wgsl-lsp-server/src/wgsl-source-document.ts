import {
  DocumentUri,
  Position,
  Range,
  TextDocumentContentChangeEvent,
} from 'vscode-languageserver';
import { computeLineOffsets, getWellformedRange } from './helpers';

export class WGSLSourceDocument {
  private _lineOffsets: number[] | undefined;

  constructor(
    private _uri: DocumentUri,
    private _version: number,
    private _content: string
  ) {}

  get uri(): DocumentUri {
    return this._uri;
  }
  get version(): number {
    return this._version;
  }
  get size(): number {
    return this._content.length;
  }

  getLineOffsets(): number[] {
    if (this._lineOffsets === undefined) {
      this._lineOffsets = computeLineOffsets(this._content, true);
    }
    return this._lineOffsets;
  }

  getText(range?: Range): string {
    if (range) {
      const start = this.offsetAt(range.start);
      const end = this.offsetAt(range.end);
      return this._content.substring(start, end);
    }
    return this._content;
  }

  offsetAt(position: Position): number {
    const lineOffsets = this.getLineOffsets();
    if (position.line >= lineOffsets.length) {
      return this._content.length;
    } else if (position.line < 0) {
      return 0;
    }
    const lineOffset = lineOffsets[position.line];
    const nextLineOffset =
      position.line + 1 < lineOffsets.length
        ? lineOffsets[position.line + 1]
        : this._content.length;
    return Math.max(
      Math.min(lineOffset + position.character, nextLineOffset),
      lineOffset
    );
  }

  positionAt(offset: number): Position {
    offset = Math.max(Math.min(offset, this._content.length), 0);
    const lineOffsets = this.getLineOffsets();
    let low = 0,
      high = lineOffsets.length;
    if (high === 0) {
      return { line: 0, character: offset };
    }
    while (low < high) {
      const mid = Math.floor((low + high) / 2);
      if (lineOffsets[mid] > offset) {
        high = mid;
      } else {
        low = mid + 1;
      }
    }
    // low is the least x for which the line offset is larger than the current offset
    // or array.length if no line offset is larger than the current offset
    const line = low - 1;
    return { line, character: offset - lineOffsets[line] };
  }

  getFullRange(): Range {
    return {
      start: { line: 0, character: 0 },
      end: this.positionAt(this._content.length),
    };
  }

  rangeForOffsets(start: number, end: number): Range {
    return {
      start: this.positionAt(start),
      end: this.positionAt(end),
    };
  }

  update(version: number, changes: TextDocumentContentChangeEvent[]): void {
    if (version < this._version) return; // updates are outdated
    if (!changes.length) return;
    for (const change of changes) {
      if (!change) continue;
      if ('range' in change) {
        this.applyRangeUpdate(change.range, change.text);
      } else if ('text' in change) {
        this.applyFullTextUpdate(change.text);
      } else {
        // eslint-disable-next-line no-console
        console.error(new Error('Unknown change event received'));
      }
    }
    this._version = version;
  }

  private applyRangeUpdate(range: Range, text: string): void {
    // makes sure start is before end
    range = getWellformedRange(range);
    // update content
    const startOffset = this.offsetAt(range.start);
    const endOffset = this.offsetAt(range.end);
    this._content =
      this._content.substring(0, startOffset) +
      text +
      this._content.substring(endOffset, this._content.length);
    // update the offsets
    const startLine = Math.max(range.start.line, 0);
    const endLine = Math.max(range.end.line, 0);
    let lineOffsets = this._lineOffsets || [];
    const addedLineOffsets = computeLineOffsets(text, false, startOffset);
    if (endLine - startLine === addedLineOffsets.length) {
      for (let i = 0, len = addedLineOffsets.length; i < len; i++) {
        lineOffsets[i + startLine + 1] = addedLineOffsets[i];
      }
    } else {
      if (addedLineOffsets.length < 10000) {
        lineOffsets.splice(
          startLine + 1,
          endLine - startLine,
          ...addedLineOffsets
        );
      } else {
        // avoid too many arguments for splice
        this._lineOffsets = lineOffsets = lineOffsets
          .slice(0, startLine + 1)
          .concat(addedLineOffsets, lineOffsets.slice(endLine + 1));
      }
    }
    const diff = text.length - (endOffset - startOffset);
    if (diff !== 0) {
      for (
        let i = startLine + 1 + addedLineOffsets.length,
          len = lineOffsets.length;
        i < len;
        i++
      ) {
        lineOffsets[i] = lineOffsets[i] + diff;
      }
    }
  }

  private applyFullTextUpdate(text: string): void {
    this._content = text;
    this._lineOffsets = undefined;
  }
}
