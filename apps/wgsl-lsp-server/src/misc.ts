import { Range } from 'vscode-languageserver';
import { WGSLSourceDocument } from './wgsl-source-document';

export function nextTick(): Promise<void> {
  return new Promise((resolve) => setTimeout(resolve, 0));
}

export function getRangeForOffset(
  document: WGSLSourceDocument,
  start: number,
  end: number
): Range {
  const startRange = document.positionAt(start);
  const endRange = document.positionAt(end);

  return { start: startRange, end: endRange };
}
