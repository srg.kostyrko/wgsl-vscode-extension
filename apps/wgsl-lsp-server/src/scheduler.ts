import { Signal } from './signal';

export class Scheduler {
  private timer: ReturnType<typeof setTimeout> | null = null;

  public readonly signal = new Signal<void>();

  constructor(private delay: number) {}

  schedule(): void {
    if (this.timer) clearTimeout(this.timer);
    this.timer = setTimeout(this.fire, this.delay);
  }

  fire = () => {
    this.signal.trigger();
  };

  dispose() {
    if (this.timer) {
      clearTimeout(this.timer);
    }
    this.signal.unbindAll();
  }
}
