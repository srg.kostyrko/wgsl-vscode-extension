import { IRecognitionException, MismatchedTokenException } from 'chevrotain';
import { Range } from 'vscode-languageserver';

export function computeLineOffsets(
  text: string,
  isAtLineStart: boolean,
  textOffset = 0
): number[] {
  const result = isAtLineStart ? [textOffset] : [];
  for (let i = 0; i < text.length; i++) {
    const ch = text.charCodeAt(i);
    if (ch === 13 /* CarriageReturn */ || ch === 10 /* LineFeed */) {
      if (
        ch === 13 /* CarriageReturn */ &&
        i + 1 < text.length &&
        text.charCodeAt(i + 1) === 10 /* LineFeed */
      ) {
        i++;
      }
      result.push(textOffset + i + 1);
    }
  }
  return result;
}

export function getWellformedRange(range: Range): Range {
  const start = range.start;
  const end = range.end;
  if (
    start.line > end.line ||
    (start.line === end.line && start.character > end.character)
  ) {
    return { start: end, end: start };
  }
  return range;
}

export function isMultiLineRange(range: Range): boolean {
  return range.start.line !== range.end.line;
}

export function splitMultilineRange(range: Range): Range[] {
  if (!isMultiLineRange(range)) return [range];

  const ranges: Range[] = [];
  for (let line = range.start.line; line <= range.end.line; line++) {
    ranges.push({
      start: {
        line,
        character: line === range.start.line ? range.start.character : 0,
      },
      end: {
        line,
        character:
          line === range.end.line
            ? range.end.character
            : Number.MAX_SAFE_INTEGER,
      },
    });
  }
  return ranges;
}

export function extractParseErrorRange(
  parseError: IRecognitionException
): [number, number] {
  if (
    parseError instanceof MismatchedTokenException &&
    isNaN(parseError.token.startOffset)
  ) {
    return [
      parseError.previousToken.startOffset,
      parseError.previousToken.startOffset +
        parseError.previousToken.image.length,
    ];
  }
  return [
    parseError.token.startOffset,
    parseError.token.startOffset + parseError.token.image.length,
  ];
}
