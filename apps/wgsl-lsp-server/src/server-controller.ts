import {
  Connection,
  Diagnostic,
  DocumentUri,
  InitializeParams,
  InitializeResult,
  SemanticTokensLegend,
  SemanticTokensRegistrationOptions,
  SemanticTokensRegistrationType,
  TextDocumentSyncKind,
} from 'vscode-languageserver';
import { WgslAstVisitor, WgslParser } from '@wgsl/parser';
import { Logger } from './logger';
import { WGSLFileManager } from './file-manager';
import { computeLegend } from './semantic-tokens/legend';

const initializeData: InitializeResult = {
  capabilities: {
    textDocumentSync: {
      openClose: true,
      change: TextDocumentSyncKind.Incremental,
    },
  },
};

export class ServerController {
  readonly parser = new WgslParser();
  readonly astVisitor = new WgslAstVisitor();

  #semanticTokensLegend!: SemanticTokensLegend;
  #logger!: Logger;

  #documentManagers = new Map<DocumentUri, WGSLFileManager>();

  constructor(private connection: Connection) {
    this.run();
  }

  run() {
    this.connection.onInitialize(
      (params: InitializeParams): InitializeResult => {
        this.#semanticTokensLegend = computeLegend(
          params.capabilities.textDocument?.semanticTokens
        );
        return initializeData;
      }
    );

    this.connection.onInitialized(() => {
      this.setupServer();
    });

    // Listen on the connection
    this.connection.listen();
  }

  private async setupServer(): Promise<void> {
    this.#logger = new Logger(this.connection.console);

    this.setupDocumentManagement();
    this.setupSemanticTokens();

    this.#logger.info('LSP Server is up and running');
  }

  private setupDocumentManagement() {
    this.connection.onDidOpenTextDocument(async (event) => {
      this.#logger.log('onDidOpenTextDocument', event.textDocument.uri);
      const { textDocument } = event;
      const manager = this.getDocumentManager(textDocument.uri);
      manager.addDocument(
        textDocument.uri,
        textDocument.version,
        textDocument.text
      );
      manager.openDocument(textDocument.uri);
    });
    this.connection.onDidChangeTextDocument(async (event) => {
      this.#logger.log('onDidChangeTextDocument', event.textDocument.uri);
      const { textDocument, contentChanges } = event;
      if (contentChanges.length === 0) {
        return;
      }

      const { version, uri } = textDocument;
      if (version === null || version === undefined) {
        this.#logger.error(
          `Received document change event for ${uri} without valid version identifier`
        );
        return;
      }
      const manager = this.getDocumentManager(uri);
      manager.updateDocument(uri, version, contentChanges);
    });
    this.connection.onDidCloseTextDocument(async (event) => {
      this.#logger.log('onDidCloseTextDocument', event.textDocument.uri);
      const { uri } = event.textDocument;
      const manager = this.getDocumentManager(uri);
      manager.closeDocument(uri);
    });
  }

  private getDocumentManager(uri: DocumentUri): WGSLFileManager {
    let manager = this.#documentManagers.get(uri);
    if (!manager) {
      manager = new WGSLFileManager(this.parser, this.astVisitor);
      manager.diagnosticSignal.bind(this.sendDiagnostics);
      manager.destroyed.bind(this.onManagerDestroyed);
      this.#documentManagers.set(uri, manager);
    }
    return manager;
  }

  private setupSemanticTokens() {
    const registrationOptions: SemanticTokensRegistrationOptions = {
      documentSelector: [{ scheme: 'file', language: 'wgsl' }],
      legend: this.#semanticTokensLegend,
      range: false,
      full: {
        delta: false,
      },
    };
    this.connection.client.register(
      SemanticTokensRegistrationType.type,
      registrationOptions
    );
    this.connection.languages.semanticTokens.on(async (params) => {
      const manager = this.getDocumentManager(params.textDocument.uri);
      return await manager.getSemainticTokens();
    });
  }

  private sendDiagnostics = (data: {
    uri: DocumentUri;
    diagnostics: Diagnostic[];
  }) => {
    this.connection.sendDiagnostics(data);
  };

  private onManagerDestroyed = (manager: WGSLFileManager) => {
    manager.destroyed.unbindAll();
    for (const [key, m] of this.#documentManagers.entries()) {
      if (m === manager) {
        this.#documentManagers.delete(key);
      }
    }
  };
}
