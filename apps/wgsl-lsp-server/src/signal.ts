type SignalCallback<T> = (value: T) => void;

export class Signal<T> {
  private slots = new Set<SignalCallback<T>>();

  bind(fn: SignalCallback<T>): void {
    this.slots.add(fn);
  }

  unbind(fn: SignalCallback<T>): void {
    this.slots.delete(fn);
  }

  unbindAll(): void {
    this.slots = new Set();
  }

  trigger(value: T): void {
    for (const fn of this.slots) {
      fn(value);
    }
  }
}
