import { ExtensionContext, window } from 'vscode';
import {
  BaseLanguageClient,
  LanguageClientOptions,
} from 'vscode-languageclient';
import { ClientLogger } from './client-logger';

export class ClientController {
  private client!: BaseLanguageClient;
  private outputChannel = window.createOutputChannel('WGSL');
  private logger = new ClientLogger(this.outputChannel);

  constructor(
    private context: ExtensionContext,
    private clientFabric: (
      context: ExtensionContext,
      clientOptions: LanguageClientOptions
    ) => BaseLanguageClient
  ) {
    this.run();
  }

  run() {
    // Options to control the language client
    const clientOptions: LanguageClientOptions = {
      // Register the server for qsp documents
      documentSelector: [{ scheme: 'file', language: 'wgsl' }],
      diagnosticCollectionName: 'wgsl-lsp-server',
      outputChannel: this.outputChannel,
    };

    this.client = this.clientFabric(this.context, clientOptions);

    this.client.start().then(() => {
      this.logger.info('WGSL LSP client started');
    });
  }

  dispose() {
    return this.client.stop();
  }
}
