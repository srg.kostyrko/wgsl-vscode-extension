import { OutputChannel } from 'vscode';

export type LogLevel = 'error' | 'log' | 'debug';

export class ClientLogger {
  private nested: ClientLogger[] = [];
  constructor(
    private outputChannel: OutputChannel,
    private level: LogLevel = 'log',
    private prefix = ''
  ) {}

  error(...args: unknown[]): void {
    this.write('error', args);
  }
  warn(...args: unknown[]): void {
    if (this.level === 'error') return;
    this.write('warn', args);
  }
  info(...args: unknown[]): void {
    if (this.level === 'error') return;
    this.write('info', args);
  }

  log(...args: unknown[]): void {
    if (this.level === 'error') return;
    this.write('log', args);
  }

  debug(...args: unknown[]): void {
    if (this.level !== 'debug') return;
    this.write('debug', args);
  }

  private write(type: string, args: unknown[]): void {
    this.outputChannel.appendLine(this.prepareArgs([`[${type}]`, ...args]));
  }

  changeLogLevel(level: LogLevel): void {
    this.level = level;
    for (const nested of this.nested) {
      nested.changeLogLevel(level);
    }
  }

  createdNested(prefix: string) {
    const nested = new ClientLogger(
      this.outputChannel,
      this.level,
      this.prefix ? `${this.prefix} > ${prefix}` : prefix
    );
    this.nested.push(nested);
    return nested;
  }

  private prepareArgs(args: unknown[]): string {
    return [
      this.prefix && `[${this.prefix}]`,
      ...args.map((arg) =>
        typeof arg === 'object' ? JSON.stringify(arg) : String(arg)
      ),
    ].filter(Boolean).join(' ');
  }
}
