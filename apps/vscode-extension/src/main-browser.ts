import { ExtensionContext, Uri } from 'vscode';
import { LanguageClientOptions } from 'vscode-languageclient';

import { LanguageClient } from 'vscode-languageclient/browser';
import { ClientController } from './client-controller';

let controller: ClientController;

function createLspClient(
  context: ExtensionContext,
  clientOptions: LanguageClientOptions
): LanguageClient {
  // Create a worker. The worker main file implements the language server.
  const serverMain = Uri.joinPath(
    context.extensionUri,
    'server/main-browser.js'
  );
  const worker = new Worker(serverMain.toString());

  return new LanguageClient(
    'wgsl-lsp',
    'WGSL Language Server',
    clientOptions,
    worker
  );
}

export function activate(context: ExtensionContext) {
  controller = new ClientController(context, createLspClient);
}

export function deactivate(): Thenable<void> | undefined {
  if (!controller) {
    return undefined;
  }
  return controller.dispose();
}
