import * as path from 'path';
import { ExtensionContext } from 'vscode';

import {
  LanguageClient,
  LanguageClientOptions,
  ServerOptions,
  TransportKind,
} from 'vscode-languageclient/node';
import { ClientController } from './client-controller';

let controller: ClientController;

function createLspClient(
  context: ExtensionContext,
  clientOptions: LanguageClientOptions
): LanguageClient {
  // The server is implemented in node
  const serverModule = context.asAbsolutePath(path.join('server', 'main.js'));
  // The debug options for the server
  // --inspect=6009: runs the server in Node's Inspector mode so VS Code can attach to the server for debugging
  const debugOptions = { execArgv: ['--nolazy', '--inspect=6009'] };

  // If the extension is launched in debug mode then the debug server options are used
  // Otherwise the run options are used
  const serverOptions: ServerOptions = {
    run: { module: serverModule, transport: TransportKind.ipc },
    debug: {
      module: serverModule,
      transport: TransportKind.ipc,
      options: debugOptions,
    },
  };

  return new LanguageClient(
    'wgsl-lsp',
    'WGSL Language Server',
    serverOptions,
    clientOptions
  );
}

export function activate(context: ExtensionContext) {
  controller = new ClientController(context, createLspClient);
}

export function deactivate(): Thenable<void> | undefined {
  if (!controller) {
    return undefined;
  }
  return controller.dispose();
}
