import {WgslLexer, parser, disambiguateTemplateLists, WgslAstVisitor} from "@wgsl/parser";
import { generateCstDts } from "chevrotain";

const button = document.querySelector<HTMLButtonElement>('#run')
button?.addEventListener('click', () => {
  doRun();
});
const textarea = document.querySelector<HTMLTextAreaElement>('#playground')
textarea?.addEventListener('change', () => {
  localStorage.setItem('last_data', textarea.value);
});
const lastData = localStorage.getItem('last_data');
if (lastData && textarea) textarea.value = lastData;

console.log(generateCstDts(parser.getGAstProductions()));
const astBuilder = new WgslAstVisitor();

function doRun(): void {
  const text = textarea?.value ?? '';

  const lexResult = WgslLexer.tokenize(text);
  const tokens = disambiguateTemplateLists(lexResult.tokens);
  console.log(tokens);
  // setting a new input will RESET the parser instance's state.
  parser.input = tokens;

  const result = parser.programm();
  console.log(result, parser.errors);
  console.log(astBuilder.visit(result));
}
